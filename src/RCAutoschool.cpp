#include "RCAutoschool.h"

RCAutoschool*
RCAutoschool::self = nullptr;

RCAutoschool*
RCAutoschool::getInstance()
{
    if(!self)
        self = new RCAutoschool();

    return self;
}

RCAutoschool::RCAutoschool()
{
    ClassName = "RCAutoschool";
	players.clear();
}

RCAutoschool::~RCAutoschool()
{
	players.clear();
}

int
RCAutoschool::init(const char* Dir)
{

    strcpy(RootDir, Dir);
	this->db = DBMySQL::Get();

	this->insim = CInsim::getInstance();
	this->msg = RCMessage::getInstance();

    CCText("^3"+ClassName+":\t^2inited");
    return 0;
}

void
RCAutoschool::ReadConfig(const char* Track)
{

	this->Track = Track;

	string confFile = StringFormat("%s/RCAutoschool/%s/config.json", RootDir, Track);

	ifstream file;

	file.open(confFile, ios::binary);

	if( !file.is_open() )
	{
		cout  << "Failed to open configuration file: " << confFile << endl;;
		return;
	}

	bool readed = configReader.parse( file, config, false );

	if ( !readed )
	{
		file.close();
		// report to the user the failure and their locations in the document.
		cout  << "Failed to parse configuration\n"
				   << configReader.getFormattedErrorMessages();
		return;
	}
	file.close();

	this->arSchoolAreaX = new int[config["school_area"].size()];
    this->arSchoolAreaY = new int[config["school_area"].size()];

    for( unsigned int j = 0; j < config["school_area"].size(); ++j )
    {
        this->arSchoolAreaX[j] = config["school_area"][j]["X"].asInt();
        this->arSchoolAreaY[j] = config["school_area"][j]["Y"].asInt();
    }

	CCText("  ^7" + ClassName + "\t^2OK");
}

void
RCAutoschool::InsimNCN( struct IS_NCN* packet )
{
    if (packet->UCID == 0)
    {
        return;
    }

    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(LastTaskTime.find(packet->UName) == LastTaskTime.end()){
        LastTaskTime[packet->UName] = 0;
    }

    if(!packet->Admin)
    {
        players[packet->UCID].Admin = this->isAdmin(packet->UName);
    }
}

void
RCAutoschool::InsimNPL( struct IS_NPL* packet )
{
	PLIDtoUCID[ packet->PLID ] = packet->UCID;
    players[packet->UCID].CName = expandCar(packet->CName);
}

void
RCAutoschool::InsimPLP( struct IS_PLP* packet )
{
}

void
RCAutoschool::InsimPLL( struct IS_PLL* packet )
{
	PLIDtoUCID.erase( packet->PLID );
}

void
RCAutoschool::InsimCNL( struct IS_CNL* packet )
{
	players.erase( packet->UCID );
}

void
RCAutoschool::InsimCPR( struct IS_CPR* packet )
{
	players[packet->UCID].PName = packet->PName;
}

void
RCAutoschool::InsimMSO( struct IS_MSO* packet )
{
	if (packet->UCID == 0)
    {
        return;
    }

    xString Message = packet->Msg + packet->TextStart;
    vector<xString> args = Message.split(' ',1);

    if(players[packet->UCID].onSchoolPlace)
	{
		if(Message == "!start")
		{
            if(time(nullptr) < LESSON_INTERVAL + LastTaskTime[ players[packet->UCID].UName])
            {
                insim->SendMTC(packet->UCID, StringFormat(msg->_(packet->UCID, "SCHOOL_WAIT"), (LESSON_INTERVAL/60)));
                return;
            }

            if(players[packet->UCID].LessonIndex > -1)
            {
                insim->SendMTC(packet->UCID, msg->_(packet->UCID, "SCHOOL_ALREADY_EXAM"));
                return;
            }

            string carCategory = RCCore::getInstance()->getCars()[players.at(packet->UCID).CName]["category"].asString();
            string documentName = "DriveLicense_" + carCategory;

            if(Items::Get()->getItemsByOwner(players.at(packet->UCID).UName,documentName).size() != 0)
            {
                insim->SendMTC(packet->UCID,StringFormat(msg->_(packet->UCID, "SCHOOL_ALREADY_GOT"), carCategory.c_str()));
                return;
            }

            players[packet->UCID].isHitedHillObj = false;
            players[packet->UCID].CanDoExercise = true;
            players[packet->UCID].LessonIndex = 0;
            players[packet->UCID].Lesson = config["lessons"][players[packet->UCID].LessonIndex].asString();

            insim->SendMTC(packet->UCID, StringFormat(msg->_(packet->UCID, "SCHOOL_GOTO_EXAM"), msg->_(packet->UCID, players[packet->UCID].Lesson)));
            return;
		}
	}

    if(players[packet->UCID].Admin)
	{
		if( Message == "!saveConf" )
		{
			string confFile = StringFormat("%s/data/RCAutoschool/%s/config.temp.json", RootDir, this->Track.c_str() );

			ofstream f;
			f.open(confFile, ios::out);
			f << configWriter.write( config );
			f.close();
		}

		int X = players[ packet->UCID ].Info.X / 65536;
		int Y = players[ packet->UCID ].Info.Y / 65536;
		int H = players[ packet->UCID ].Info.Heading / 182;

		if(args.size() > 1 && args[0] == "!stopCount" && players[packet->UCID].Lesson.length() != 0 )
		{
			config["lesson"][players[packet->UCID].Lesson]["stopCount"] = args[1];

			insim->SendMTC(packet->UCID, "Set stop count for " + players[packet->UCID].Lesson );
		}

		if(Message == "!start" && players[packet->UCID].Lesson.length() != 0 )
		{
			config["lesson"][players[packet->UCID].Lesson]["start"]["X"] = X;
			config["lesson"][players[packet->UCID].Lesson]["start"]["Y"] = Y;
			config["lesson"][players[packet->UCID].Lesson]["start"]["Heading"] = H;

			insim->SendMTC(255, "Set start position for " + players[packet->UCID].Lesson );

		}

		if(Message == "!finish" && players[packet->UCID].Lesson.length() != 0 )
		{
			config["lesson"][players[packet->UCID].Lesson]["finish"]["X"] = X;
			config["lesson"][players[packet->UCID].Lesson]["finish"]["Y"] = Y;
			config["lesson"][players[packet->UCID].Lesson]["finish"]["Heading"] = H;

			insim->SendMTC(255, "Set finish position for " + players[packet->UCID].Lesson );
		}

		if(Message == "!check" && players[packet->UCID].Lesson.length() != 0 )
		{
			Json::Value point;

			point["X"] = X;
			point["Y"] = Y;
			point["Heading"] = H;

			config["lesson"][players[packet->UCID].Lesson]["checkpoints"].append( point );

			insim->SendMTC(255, "Add checkpoint for " + players[packet->UCID].Lesson );
		}

		if(args[0] == "!lesson")
		{
			if( args.size() > 1 )
			{
				for( auto l: config["lessons"] )
				{
					if( l.asString() == args[1] )
					{
						players[packet->UCID].Lesson = args[1];
						insim->SendMTC(255, l.asString() );
						return;
					}
				}
				for( auto l: config["lessons"] )
				{
					insim->SendMTC(255, l.asString() );
				}
			}

			else{
				insim->SendMTC(255, "Lesson not selected");

				for( auto l: config["lessons"] )
				{
					insim->SendMTC(255, l.asString() );
				}
			}
		}
	}
}

void
RCAutoschool::InsimCON( struct IS_CON* packet )
{
	byte UCIDA = PLIDtoUCID[ packet->A.PLID ];
    byte UCIDB = PLIDtoUCID[ packet->B.PLID ];

    CancelLesson(UCIDA);
    CancelLesson(UCIDB);
}

void
RCAutoschool::InsimOBH( struct IS_OBH* packet )
{
	 byte UCID = PLIDtoUCID[ packet->PLID ];

	 //insim->SendMTC(UCID, ToString(packet->Index));

	 // ���� ����������� ���� ��� ����� ������� �� �����
	 if(packet->Index == 138 && players.at(UCID).Lesson == "hill" && players.at(UCID).isHitedHillObj == false)
     {
         players.at(UCID).isHitedHillObj = true;
         return;
     }

	 if(packet->Index != 172 && packet->Index != 173)
        CancelLesson(UCID);
}

void
RCAutoschool::InsimHLV( struct IS_HLV* packet )
{
	 byte UCID = PLIDtoUCID[ packet->PLID ];

    /** ������������ �� ������ **/
    if (packet->HLVC==1)
    {
		CancelLesson(UCID);
    }
}

void
RCAutoschool::InsimAXM( struct IS_AXM* packet )
{
	if( packet->UCID == 0 )
		return;

	if( packet->PMOAction != PMO_ADD_OBJECTS )
		return;

	//CCText("^3RCAutoschool^1::^2InsimAXM");

}

void
RCAutoschool::InsimMCI ( struct IS_MCI* packet )
{
    for (int i = 0; i < packet->NumC; i++) // ������ �� ����� ������� packet->Info[i]
    {
        byte UCID = PLIDtoUCID[ packet->Info[i].PLID ];

        int X = packet->Info[i].X/65536;
        int Y = packet->Info[i].Y/65536;
        int Speed = ((int)packet->Info[i].Speed*360)/(32768);



        if( CheckPosition(config["school_area"].size(),this->arSchoolAreaX,this->arSchoolAreaY,X,Y) )
        {

            if(!players[UCID].onSchoolPlace)
			{
				players[UCID].onSchoolPlace = true;

				string carCategory = RCCore::getInstance()->getCars()[players.at(UCID).CName]["category"].asString();
				string documentName = "DriveLicense_" + carCategory;

                if(Items::Get()->getItemsByOwner(players.at(UCID).UName,documentName).size() == 0)
                {
                    insim->SendMTC(UCID, msg->_(UCID, "SCHOOL_AREA_WELCOME"));
                    insim->SendMTC(UCID, StringFormat(msg->_(UCID, "SCHOOL_AREA_WELCOME2"),carCategory.c_str()));
                    insim->SendMTC(UCID, msg->_(UCID, "SCHOOL_AREA_START"));
                }
                else
                {
                    insim->SendMTC(UCID,StringFormat(msg->_(UCID, "SCHOOL_ALREADY_GOT"), carCategory.c_str()));
                }
			}

            if(players[UCID].CanDoExercise && !players[UCID].Lesson.empty())
            {

				ShowPanel(UCID);

				Json::Value lesson = config["lesson"][ players[UCID].Lesson ];

				int dStartHeading, dFinishHeading;

				dStartHeading = ( lesson["start"]["Heading"].asInt() - (int)(players[UCID].Info.Heading/182) );
				dStartHeading = ( dStartHeading < 0 ) ? dStartHeading + 360 : dStartHeading;
				dStartHeading = ( dStartHeading > 180 ) ? 360 - dStartHeading : dStartHeading;

				dFinishHeading = ( lesson["finish"]["Heading"].asInt() - (int)(players[UCID].Info.Heading/182) );
				dFinishHeading = ( dFinishHeading < 0 ) ? dFinishHeading + 360 : dFinishHeading;
				dFinishHeading = ( dFinishHeading > 180 ) ? 360 - dFinishHeading : dFinishHeading;

				int StartDist = Distance(X, Y, lesson["start"]["X"].asInt(), lesson["start"]["Y"].asInt() ),
				PointDist = 0,
				FinishDist = Distance(X, Y, lesson["finish"]["X"].asInt(), lesson["finish"]["Y"].asInt() );

				int* arX = new int[ lesson["position"].size() ];
				int* arY = new int[ lesson["position"].size() ];

				for( unsigned int j = 0; j < lesson["position"].size(); ++j )
				{
					arX[j] = lesson["position"][j]["X"].asInt();
					arY[j] = lesson["position"][j]["Y"].asInt();
				}

				if( CheckPosition(lesson["position"].size(),arX,arY,X,Y) )
				{
					if( !players[UCID].OnPlace )
						insim->SendMTC(UCID, msg->_(UCID,"SCHOOL_EXERCISE_PREPARE") );

					players[UCID].OnPlace = true;

					if( StartDist < 15 && dStartHeading < 15  && Speed == 0 && players[UCID].CheckPoint == 0)
					{
						if( !players[UCID].Started )
							insim->SendMTC(UCID, msg->_(UCID,"SCHOOL_EXERCISE_START") );

						players[UCID].Started = true;
					}


					if( players[UCID].Started && players[UCID].CheckPoint < lesson["checkpoints"].size() )
					{
						Json::Value point = lesson["checkpoints"][ players[UCID].CheckPoint ];

						PointDist = Distance(X, Y, point["X"].asInt(), point["Y"].asInt() );

						if( PointDist < 2 )
							players[UCID].CheckPoint++;

					}

					if( players[UCID].CheckPoint == lesson["checkpoints"].size() )
					{
						if( FinishDist < 2 && dFinishHeading < 5  && Speed == 0)
						{
							if( !players[UCID].Finished )
							{
                                players[UCID].Finished = true;
                                NextLesson(UCID);
                            }
						}
					}


				}
				else
				{
					players[UCID].OnPlace = false;
					players[UCID].Finished = false;
					players[UCID].Started = false;
					players[UCID].CheckPoint = 0;
				}

				delete arX;
				delete arY;

			}
        } else {
            players[UCID].onSchoolPlace = false;
        }

        players[UCID].Info = packet->Info[i];
    }
}

void
RCAutoschool::ShowPanel(byte UCID)
{
    if(players[UCID].CanDoExercise && players[UCID].OnPlace)
    {
        byte ClickID = CLICK_ID_130;

        byte Left = 100;
        byte Top = 10;
        byte Width = 50;
        byte Height = 4;

        Json::Value lesson = config["lesson"][ players[UCID].Lesson ];

        insim->SendButton(255,UCID,ClickID++,Left-1,Top + 4,Width+2,Height*2+3,ISB_LIGHT,"");
        insim->SendButton(255,UCID,ClickID++,Left,Top += 5,Width,Height,ISB_DARK, StringFormat(msg->_(UCID, "SCHOOL_PANEL_EXERCISE"), msg->_(UCID, players[UCID].Lesson)));
        insim->SendButton(255,UCID,ClickID++,Left,Top += 5,Width,Height,ISB_DARK,StringFormat(msg->_(UCID, "SCHOOL_PANEL_CHECKPOINT"), players[UCID].CheckPoint, lesson["checkpoints"].size() ));
    }
}

void
RCAutoschool::HidePanel(byte UCID)
{
    insim->SendBFN(UCID,CLICK_ID_130, 134);
}

void
RCAutoschool::NextLesson(byte UCID)
{
    if(time(nullptr) < LESSON_INTERVAL + LastTaskTime[ players[UCID].UName])
    {
        insim->SendMTC(UCID, StringFormat(msg->_(UCID, "SCHOOL_WAIT"), (LESSON_INTERVAL/60)));
        return;
    }

    if(players[UCID].LessonIndex < 0)
    {
        insim->SendMTC(UCID, msg->_(UCID, "SCHOOL_AREA_START"));
        return;
    }

    if(players[UCID].CanDoExercise && players[UCID].OnPlace)
    {
        players[UCID].LessonIndex++;

        if(players[UCID].LessonIndex == config["lessons"].size())
        {


            string carCategory = RCCore::getInstance()->getCars()[players.at(UCID).CName]["category"].asString();
            string documentName = "DriveLicense_" + carCategory;

            int id = Items::Get()->Add(documentName,"");
            if(id)
            {
                insim->SendMTC(UCID, StringFormat(msg->_(UCID, "SCHOOL_SUCCESS"), carCategory.c_str()));
                Items::Get()->GetItem(id)->setOwner(players.at(UCID).UName)->Save();
                this->HidePanel(UCID);
            }
            return;
        }

        players[UCID].CanDoExercise = true;
        players[UCID].Started = false;
        players[UCID].Finished = false;
        players[UCID].CheckPoint = 0;
        players[UCID].isHitedHillObj = false;


        players[UCID].Lesson = config["lessons"][players[UCID].LessonIndex].asString();

        insim->SendMTC(UCID, StringFormat(msg->_(UCID, "SCHOOL_GOTO_EXAM"), msg->_(UCID, players[UCID].Lesson)));
    }
    return;
}

void
RCAutoschool::CancelLesson(byte UCID)
{
    if(players[UCID].CanDoExercise && players[UCID].OnPlace)
    {
        insim->SendMTC(UCID, msg->_(UCID, "SCHOOL_FAIL"));

        players[UCID].CanDoExercise = false;
        players[UCID].OnPlace = false;
        players[UCID].Started = false;
        players[UCID].Finished = false;
        players[UCID].CheckPoint = 0;
        players[UCID].isHitedHillObj = false;
        players[UCID].LessonIndex = -1;
        players[UCID].Lesson = "";
        LastTaskTime[ players[UCID].UName] = time(nullptr);
        HidePanel(UCID);
    }
}

