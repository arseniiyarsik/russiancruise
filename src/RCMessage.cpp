using namespace std;

#include "RCMessage.h"

RCMessage*
RCMessage::self = nullptr;

RCMessage*
RCMessage::getInstance()
{
    if(!self)
        self = new RCMessage();

    return self;
}

RCMessage::RCMessage()
{
    ClassName = "RCMessage";
}

RCMessage::~RCMessage()
{

}

const char* RCMessage::_( byte UCID, string CODE )
{
    if(DEFAULT_LANGUAGE.empty())
        DEFAULT_LANGUAGE = COptions::getString(ClassName, "DEFAULT_LANGUAGE");

    if ( players[ UCID ].Lang.size() == 0)
	{
		if ( MsgArray[ DEFAULT_LANGUAGE ].find( CODE ) != MsgArray[ DEFAULT_LANGUAGE ].end() )
        {
            return MsgArray[ DEFAULT_LANGUAGE ][ CODE ].c_str();
        }
        return CODE.c_str();
	}

    if ( MsgArray[ players[ UCID ].Lang ].find( CODE ) != MsgArray[ players[ UCID ].Lang ].end() )
        return MsgArray[ players[ UCID ].Lang ][ CODE ].c_str();

    if ( MsgArray[ DEFAULT_LANGUAGE ].find( CODE ) != MsgArray[ DEFAULT_LANGUAGE ].end() )
    {
        CCText("^1Return default language message with code " + CODE + " to " + players[ UCID ].UName );
        return MsgArray[ DEFAULT_LANGUAGE ][ CODE ].c_str();
    }

    return CODE.c_str();
}

string RCMessage::GetLang(byte UCID)
{
    return players[UCID].Lang;
}

int RCMessage::init(const char* Dir)
{
    strcpy(RootDir,Dir);

    this->db = DBMySQL::Get();
    if (!this->db)
    {
        printf("RCMessage: Can't sctruct MySQL Connector\n");
        return -1;
    }

    insim = CInsim::getInstance();
    if (!insim)
    {
        printf ("RCMessage: Can't struct CInsim class");
        return -1;
    }

    CCText("^3"+ClassName+":\t^2inited");
    return 0;
}

void RCMessage::ReadConfig(const char *Track)
{

    DEFAULT_LANGUAGE = COptions::getString(this->ClassName, "DEFAULT_LANGUAGE");

	ReadLangDir(StringFormat("%s/%s", RootDir, this->ClassName.c_str()));

	ReadSwearCharList();

	ReadSwearList();

    CCText("  ^7RCMessage\t^2OK");
}

void RCMessage::ReadSwearCharList()
{
    char file[255];
    sprintf(file, "%s/RCMessage/chars.txt", RootDir);

    ifstream readf (file, ios::in);

    if(!readf.is_open())
    {
        cout << "Can't find " << file << endl;
        return;
    }

    SwearList.clear();

    while (readf.good())
    {
        char str[8];
        readf.getline(str, 8);

        if (strlen(str) > 1)
        {
            char * A = strtok(str, "= ");
            char * a = strtok(NULL, "= ");
            CharList[A[0]] = a[0];
        }
    }


    readf.close();
}

void RCMessage::ReadSwearList()
{
    string file = StringFormat("%s/RCMessage/swears.txt", RootDir);

    ifstream readf (file, ios::in);

    if(!readf.is_open())
    {
        cout << "Can't find " << file << endl;
        return;
    }

    SwearList.clear();

    while (readf.good())
    {
        char str[128];
        readf.getline(str, 128);

        if (strlen(str) > 1)
            SwearList.push_back(str);
    }

    readf.close();
}

void
RCMessage::ReadLangDir(xString path)
{
    DIR *dp;
    struct dirent *dent;

    if( (dp = opendir(path.c_str())) == NULL) {
        printf("opendir: %s: %s\n", path.c_str(), strerror(errno));
        return;
    }

    dent = readdir(dp);
    while(dent != nullptr)
    {
        if( strlen( dent->d_name ) == 6 )
        {
            string fileName = StringFormat("%s/%s", path.c_str(), dent->d_name );

            ReadLangFile( fileName );
        }
        dent = readdir(dp);
    }

    closedir(dp);
	return;
}

void
RCMessage::ReadLangFile(xString file)
{
	vector<xString> arFiles = file.split('/', 1);

    string lang = arFiles.back().substr( 0 , arFiles.back().find(".txt") );

    ifstream readf (file, ios::in);

    if(readf.is_open() == false)
    {
        CCText("^1 Can't open " + file);
        return;
    }

    while (readf.good())
    {
        char str[128];

        readf.getline(str, 128);

        if (strlen(str) > 1)
        {

			if (!strncmp(str, "#", 1) || !strncmp(str, "//", 2) || !strncmp(str, "::", 2))
				continue;

            string id;
            string mesage;

            id = strtok (str, "\"");

            if( id.size() == 0 )
			{
				CCText("RCMesages: Bad code");
				continue;
			}

            id.erase(id.find_last_not_of(" \t\r\n\0")+1);

            mesage = strtok (NULL, "\"");

			if( mesage.size() == 0 )
			{
				CCText("RCMesages: Bad message");
				continue;
			}

            mesage.erase( mesage.find_last_not_of(" \t\r\n\0")+1);

            MsgArray[ lang ].erase( id );
            MsgArray[ lang ][ id ] = mesage;
        }
    }
    readf.close();
}

void RCMessage::InsimNCN( struct IS_NCN* packet )
{
    if (packet->UCID == 0)
        return;

    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].Admin = packet->Admin;

    DB_ROWS result = db->select({}, "message", {{"username",packet->UName}});

    if( result.size() > 0 )
	{
		DB_ROW row = result.front();
		players[ packet->UCID ].Lang = row["lang"];
	}
}

void RCMessage::InsimNCI( struct IS_NCI* packet )
{

    if( players[ packet->UCID ].Lang.size() == 0)
    {
        string ln = insim->GetLanguageCode(packet->Language);

        if(MsgArray.find( ln ) == MsgArray.end())
        {
            ln = DEFAULT_LANGUAGE;
        }

        players[ packet->UCID ].Lang = ln;
        Save( packet->UCID );
    }
}

void RCMessage::InsimCNL( struct IS_CNL* packet )
{
    Save(packet->UCID);
    players.erase( packet->UCID );
}

string RCMessage::GetLangList()
{
    string lang = "";
    for( auto& l: MsgArray )
        lang += ", " + l.first;
    return lang.substr(2);
}

list<string>
RCMessage::GetLangArray()
{
    list<string> res;
    res.clear();
    for( auto& l: MsgArray )
        res.push_back(l.first);

    return res;
}

int RCMessage::CheckSwear(string Text)
{
    /** ������� ������ �� ������ ������ */
    Text = " " + StripText(Text);

    string p = ".,!?*�^_-=+#$%&:;/|<>()[]{}'\"\\";

    for(auto i = Text.begin(); i < Text.end(); i++)
        for(auto j = p.begin(); j != p.end(); j++)
            if(*i == *j)
                Text.erase(i--);

    //
    //{
    //  TODO: �������������� ���� r=�, u=�, t=�, 1=�, 9=�, 0=�, @=� � �.�. � �.�.
    //}
    //
    //{
    //  TODO: ���������� ���� ������ � ������� �������� (��� ����������� boost, �.�. ���������)
    //}
    // � �� ��� ��� ��� �������� �����!!!!!!!111:


    for(int i = 0; i < Text.length(); i++)
        if (CharList.find(Text[i]) != CharList.end())
            Text[i] = CharList[Text[i]];

    /** ����� ������ �� ������ �����, ���-�� ���������� */

    int count = 0;
    for (int i = 0; i < SwearList.size(); i++)
        if (Text.find(SwearList[i]) != string::npos)
            count++;

    return count;
}

void RCMessage::InsimMSO( struct IS_MSO* packet )
{
    if (packet->UCID == 0)
        return;

    xString Msg = packet->Msg + packet->TextStart;

    if (Msg.find("!lang") == 0 )
    {
        if ( Msg.size() < 8)
        {
            insim->SendMTC( packet->UCID, _( packet->UCID , "2104"));
            return;
        }

        vector<xString> args = Msg.split(' ');

		if(args.size() < 2)
        {
            insim->SendMTC(packet->UCID, _( packet->UCID , "2105"));
            return;
        }

        string id = args[1];

		for( auto& l: MsgArray )
		{
			if( l.first == id )
			{
                players[ packet->UCID ].Lang = id;

				insim->SendMTC(packet->UCID, _(packet->UCID, "^1| ^7Language: ") + id);
				return;
			}
		}

		insim->SendMTC(packet->UCID, StringFormat(_(packet->UCID, "^1| ^7Language %s not found"), id.c_str()));
		return;
    }
}

void
RCMessage::InsimBTC( struct IS_BTC* packet )
{
    if (packet->ClickID == 211)
    {
        ShowNotify(packet->UCID);
        if(players[packet->UCID].notifications.empty())
        {
            insim->SendBFN(packet->UCID, 211);
        }
    }
}

void RCMessage::Save (byte UCID)
{
    char query[MAX_PATH];
    sprintf(query,"REPLACE INTO message (username, lang) VALUES ('%s','%s')", players[ UCID ].UName.c_str(), players[ UCID ].Lang.c_str());

    db->exec( query );
}

void
RCMessage::AddNotify(byte UCID, string Notify)
{
    players[UCID].notifications.push(Notify);
}

void
RCMessage::ShowNotify(byte UCID)
{
    if(players[UCID].notifications.size() == 0)
        return;

    byte
    l=100, t=90,
    hButton=5,
    w=100,
    h=16+hButton;

    insim->SendButton(255, UCID, 176, l - w / 2, t - h / 2, w, h, ISB_DARK, "");
    insim->SendButton(255, UCID, 177, l - w / 2, t - h / 2, w, h, ISB_DARK, "");
    insim->SendButton(255, UCID, 178, l - w / 2, t - h / 2 + 2, w, 10, 0, players[UCID].notifications.front());
    insim->SendButton(254, UCID, 179, l - 7, t - h / 2 + 14, 14, 6, ISB_LIGHT + ISB_CLICK, "^2OK");

    players[UCID].notifications.pop();
}

void
RCMessage::Event()
{
    if(color != "1")
        color = "1";
    else
        color = "2";

    for(auto pl: players)
    {
        byte UCID = pl.first;
        if(!players[UCID].notifications.empty())
        {
            insim->SendButton(255, UCID, 211, 96, 180, 8, 8, ISB_CLICK + ISB_LIGHT, "^"+color+"^K��");
        }
    }
}

void
RCMessage::SaveAll()
{
    for( auto i: players)
    {
        Save(i.first);
    }
}
