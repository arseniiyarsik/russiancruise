#include "RCPolice.h"

RCPolice*
RCPolice::self = nullptr;

RCPolice*
RCPolice::getInstance()
{
    if(!self)
        self = new RCPolice();

    return self;
}

RCPolice::RCPolice()
{
    ClassName = "RCPolice";
}

RCPolice::~RCPolice()
{

}

int RCPolice::init(const char* Dir)
{
    strcpy(RootDir,Dir);
    this->db = DBMySQL::Get();
    if (!this->db)
    {
        CCText("^3RCPolice:\t\t^1Can't sctruct MySQL Connector");
        return -1;
    }
    insim = CInsim::getInstance();
    if (!insim)
    {
        CCText("^3RCPolice:\t\t^1Can't struct CInsim class");
        return -1;
    }

    msg = RCMessage::getInstance();
    if (!msg)
    {
        CCText("^3RCPolice:\t^1Can't struct RCMessage class");
        return -1;
    }

    bank = RCBank::getInstance();
    if (!bank)
    {
        CCText("^3RCPolice:\t^1Can't struct RCBank class");
        return -1;
    }

    dl = RCDL::getInstance();
    if (!dl)
    {
        CCText("^3RCPolice:\t^1Can't struct RCDL class");
        return -1;
    }

    street = RCStreet::getInstance();
    if (!street)
    {
        CCText("^3RCPolice:\t^1Can't struct RCStreet class");
        return -1;
    }

    nrg = RCEnergy::getInstance();
    if (!nrg)
    {
        CCText("^3RCPolice:\t^1Can't struct RCEnergy class");
        return -1;
    }

    lgh = RCLight::getInstance();
    if (!lgh)
    {
        CCText("^3RCPolice:\t^1Can't struct RCLight class");
        return -1;
    }

    CCText("^3"+ClassName+":\t^2inited");
    return 0;
}

void RCPolice::InsimNCN( struct IS_NCN* packet )
{
    if ( packet->UCID == 0 )
    {
        return;
    }

    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
        players[packet->UCID].Admin = this->isAdmin(packet->UName);
    }

    //������ ������
    ReadUserFines(packet->UCID);
}

void RCPolice::InsimNPL( struct IS_NPL* packet )
{
    byte UCID = packet->UCID;
    PLIDtoUCID[packet->PLID] = packet->UCID;
    players[packet->UCID].CName = packet->CName;
    players[UCID].onTrack = true;

    if ( ArestPlayers.end() != ArestPlayers.find( players[ packet->UCID ].UName ) )
    {
        time_t now = time(NULL);

        if (isArested(packet->UCID))
        {
            //insim->SendJRR(JRR_REJECT, packet->UCID);

            insim->SendMTC(packet->UCID, StringFormat("^2| ^7^C�� ����������. �� ����� ������ �������� ^1%d ^7�����", (int)((ArestPlayers[ players[packet->UCID].UName ] - now) / 60 + 1)));

            return;
        }

    }


    if (this->isValidPrefix(players[packet->UCID].PName) && !players[packet->UCID].cop)
    {

        players[packet->UCID].Rank = 0;

        if ( LoadCopStat(packet->UCID) && players[packet->UCID].Rank > 0)
        {
            players[packet->UCID].cop = true;
            insim->SendMTC(packet->UCID, "^2| ^7^C�� ��������� �� �����");

            insim->SendMST("/cansiren " + players[packet->UCID].UName + " 1");

            if (!((strncmp("DPS", packet->SName, 3) == 0)
                    || (strncmp("dps", packet->SName, 3) == 0)
                    || (strncmp("POLICE", packet->SName, 6) == 0)
                    || (strncmp("police", packet->SName, 6) == 0)))
                insim->SendMTC(packet->UCID, "^2| ^C^7� �������� ����� �� ������� ���������� �� �������������� � ���");


            players[packet->UCID].StartWork = time(NULL) - 5;


            players[UCID].PStat.DateActive = time(NULL);

            players[UCID].Barier.canAdd = true;

            tm *timeStruct = tools::GetLocalTime();

            if (players[UCID].PStat.CurrentDay != timeStruct->tm_mday)
            {
                players[UCID].PStat.CurrentDay = timeStruct->tm_mday;
                players[UCID].PStat.ArrestWithFineByDay = 0;
                players[UCID].PStat.ArrestWithOutFineByDay = 0;
                players[UCID].PStat.SolvedIncedentsByDay = 0;
                players[UCID].PStat.FinedByDay = 0;
                players[UCID].PStat.CanceledFinesByDay = 0;
                players[UCID].PStat.HelpArrestByDay = 0;
            }
        }
        else
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID , "CopFalse"));
            insim->SendMST("/spec " + players[packet->UCID].UName);
        }
    }

    if (players[packet->UCID].cop)
    {
        int workTime = time(nullptr) + COP_WORK_TIME;
        dl->Lock(packet->UCID);
        nrg->LockTo(packet->UCID,workTime);
        lgh->SetLightCop(packet->UCID, true);
    }
}

void RCPolice::InsimPLP( struct IS_PLP* packet )
{
    byte UCID = PLIDtoUCID[packet->PLID];
    players[UCID].onTrack = false;

    if (players[UCID].Pogonya != 0)
    {
        players[UCID].Pogonya = 2;
        RemoveBarier(UCID);
    }

    players[UCID].Sirena = false;
    if (players[UCID].Radar.On)
        RadarOff(UCID);

    lgh->SetLightCop(UCID, false);
    dl->Unlock(UCID);

    if (players[UCID].cop)
    {
        SaveCopStat(UCID);
        nrg->LockTo(UCID);
    }

    //PLIDtoUCID.erase(packet->PLID);
}

void RCPolice::InsimPLL( struct IS_PLL* packet )
{
    byte UCID = PLIDtoUCID[packet->PLID];
    players[UCID].onTrack = false;

    if (players[UCID].Pogonya != 0)
    {
        players[UCID].Pogonya = 2;
        RemoveBarier(UCID);
    }

    players[UCID].Sirena = false;
    if (players[UCID].Radar.On)
        RadarOff(UCID);

    lgh->SetLightCop(UCID, false);
    dl->Unlock(UCID);

    if (players[UCID].cop)
    {
        SaveCopStat(UCID);
        nrg->LockTo(UCID);
    }

    PLIDtoUCID.erase(packet->PLID);
}

void RCPolice::InsimCNL( struct IS_CNL* packet )
{
    for (int i = 0; i < 32; i++)
    {
        if (DTPvyzov[0][i] > 0)
        {
            if (DTPvyzov[2][i] == packet->UCID)
            {
                DTPvyzov[1][i] = time(NULL) + 2 * 60;
                DTPvyzov[2][i] = 0;
                break;
            }

            if (DTPvyzov[0][i] == packet->UCID)
            {
                players[DTPvyzov[0][i]].DTP = 0;

                if (DTPvyzov[2][i] != 0)
                {
                    players[DTPvyzov[2][i]].DTPstatus = 0;
                }

                DTPvyzov[0][i] = 0;
                DTPvyzov[1][i] = 0;
                DTPvyzov[2][i] = 0;
                insim->SendBFN(255, 91 + i);
                break;
            }
        }
    }

    if (players[packet->UCID].cop)
        insim->SendMST("/cansiren " + players[packet->UCID].UName + " 0");
    CopPayRoll(packet->UCID, false);
    RemoveBarier(packet->UCID);

    Save(packet->UCID);
    players.erase(packet->UCID);
}

void RCPolice::InsimCPR( struct IS_CPR* packet )
{
    players[packet->UCID].PName = packet->PName;

    if (players[packet->UCID].cop && !this->isValidPrefix(packet->PName) )
    {
        SaveCopStat(packet->UCID);
        CopPayRoll(packet->UCID, false);
        insim->SendBFN(packet->UCID, 70);
        players[packet->UCID].cop = false;
        players[packet->UCID].StartWork = 0;
        players[packet->UCID].Rank = 0;
        lgh->SetLightCop(packet->UCID, false);
        dl->Unlock(packet->UCID);
        nrg->LockTo(packet->UCID);
        insim->SendMTC(packet->UCID, "^2| ^7^C�� ������ �� ��������� ���");
        insim->SendMST("/cansiren " + players[packet->UCID].UName + " 0");
        insim->SendBFNAll(packet->UCID);

        for (int i = 0; i < 32; i++)
        {
            if (DTPvyzov[0][i] > 0)
            {
                if (DTPvyzov[2][i] == packet->UCID)
                {
                    DTPvyzov[1][i] = time(NULL) + 2 * 60;
                    DTPvyzov[2][i] = 0;
                    break;
                }

                if (DTPvyzov[0][i] == packet->UCID)
                {
                    players[DTPvyzov[0][i]].DTP = 0;

                    if (DTPvyzov[2][i] != 0)
                    {
                        players[DTPvyzov[2][i]].DTPstatus = 0;
                    }

                    DTPvyzov[0][i] = 0;
                    DTPvyzov[1][i] = 0;
                    DTPvyzov[2][i] = 0;
                    insim->SendBFN(255, 91 + i);
                    break;
                }
            }
        }

        for (int i=214; i <= 222; i++)
            insim->SendBFN(packet->UCID, i);
    }
}

bool RCPolice::IsPursuit(byte UCID)
{
    return players[UCID].Pogonya > 0;
}

void RCPolice::InsimMSO( struct IS_MSO* packet )
{
    if (packet->UCID == 0)
        return;

    if ( packet->UserType != MSO_PREFIX )
        return;

    byte UCID = packet->UCID;

    xString Message = packet->Msg + packet->TextStart;
    vector<xString> args = Message.split(' ',1);

    if (Message == "!911" || Message == "!dtp" || Message == "!^C���")
    {
        if (players[UCID].cop)
            return;

        if (!players[UCID].onTrack)
            return;

        if (GetCopCount() <= 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "2102"));
            return;
        }

        if (players[UCID].Pogonya > 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "WherePog"));
            return;
        }

        if (players[UCID].DTP != 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "WhereDtp"));
            return;
        }

        if (players[UCID].Info.Speed * 360 / 32768 > 0)
        {
            insim->SendMTC(UCID, msg->_(UCID, "NoDtpWhenSp"));
            return;
        }

        if (time(NULL)-players[UCID].LastDtpTime < 60*5)
        {
            insim->SendMTC(UCID, msg->_(UCID, "2106"));
            return;
        }

        players[UCID].blame = false;
        players[UCID].LastDtpTime = time(NULL);

        players[UCID].DTP = 255;

        for (int i = 0; i < 32; i++)
        {
            if (DTPvyzov[0][i] <= 0)
            {
                DTPvyzov[0][i] = (int)UCID; 		    //��� ������
                DTPvyzov[1][i] = time(NULL) + 5 * 60;	//����� ������

                insim->SendMTC(UCID, msg->_(UCID, "2107"));
                insim->SendMTC(UCID, msg->_(UCID, "2108"));

                SendMTCToCop(StringFormat(msg->_(UCID, "2109"), players[UCID].PName.c_str()), 1, 2, 4);
                return;
            }
        }

        insim->SendMTC(UCID, msg->_(UCID, "2110"));
    }


    if (Message == "!fines" || Message == "!^C������")
    {
        if(players[packet->UCID].fines.size() == 0)
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID , "NoFines"));
            return;
        }

        insim->SendMTC(packet->UCID, StringFormat(msg->_(packet->UCID , "FinesListH"), players[packet->UCID].fines.size()));

        for (auto f: players[UCID].fines)
        {
            user_fine fine = f.second;

            string CopName = "";

            if (fine.CopPName != "")
                CopName = fine.CopPName;
            else if (fine.CopUName != "")
                CopName += fine.CopUName;

            time_t t = (time_t)fine.fine_date;
            struct tm* timeinfo = localtime (&t);
            char Date[64];
            strftime(Date, 128, "%d.%m.%y", timeinfo);

            insim->SendMTC(UCID, StringFormat("^1| ^7Id ^3%02d ^7- %.64s (%d ^3RUR^7), ^2%s^7%s",
                                      fine.fine_id,
                                      GetFineName(UCID, fine.fine_id).c_str(),
                                      GetFineCash(fine.fine_id),
                                      Date,
                                      CopName != "" ? (", " + CopName).c_str() : "")
                           );
        }
    }

    if (Message.find("!pay") == 0 || Message.find("!^C��������") == 0)
    {
        if ( args.size() < 2)
        {
            insim->SendMTC( packet->UCID , msg->_(  packet->UCID , "2105" ));
            return;
        }

        int id_i = args[1].asInt();

        if ( id_i < 1 || GetFineCash(id_i) <= 0)
        {
            insim->SendMTC( packet->UCID , msg->_(  packet->UCID , "2105" ));
            return;
        }

        if(players[ packet->UCID ].fines.size() == 0)
        {
            insim->SendMTC( packet->UCID, msg->_(  packet->UCID , "NoFine" ));
            return;
        }

        if (GetFineCash(id_i) >= bank->GetCash( packet->UCID ))
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "NoManyPay"));
            return;
        }

        if (!bank->InBank( packet->UCID ))
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID, "NoOnBank"));
            return;
        }

        for (auto f = players[ packet->UCID ].fines.begin(); f != players[ packet->UCID ].fines.end(); ++f)
        {
            if (f->second.fine_id == id_i)
            {
                int fineCash = GetFineCash(id_i);
                int cop = 0;
                for ( auto& play: players)
                {
                    if ( players[ play.first ].cop && packet->UCID != play.first)
                    {
                        if (dl->Islocked(  play.first  ))
                        {
                            dl->Unlock( play.first );
                            dl->AddSkill( play.first , 0.05);
                            dl->Lock( play.first );
                        }
                        else
                        {
                            dl->AddSkill( play.first , 0.05);
                        }

                        bank->AddCash( play.first , fineCash * 0.05, true);
                        cop++;
                    }
                }

                insim->SendMTC( packet->UCID , msg->_(  packet->UCID , "PayDone" ));
                bank->RemCash( packet->UCID , fineCash);
                bank->AddToBank(fineCash - (fineCash * 0.05) * cop);

                if (dl->Islocked( packet->UCID ))
                {
                    dl->Unlock( packet->UCID );
                    dl->RemSkill( packet->UCID );
                    dl->Lock( packet->UCID );
                }
                else
                {
                    dl->RemSkill( packet->UCID );
                }

                players[ packet->UCID ].fines.erase(f->second.fine_date);

                break;
            }
        }
    }

    if (Message == "!sirena" || Message == "!^C������")
    {
        if (players[packet->UCID].cop)
        {
            if (players[packet->UCID].Sirena)
            {
                insim->SendMTC(packet->UCID, msg->_(packet->UCID, "2100"));
                players[packet->UCID].Sirena = false;
            }
            else
            {
                insim->SendMTC(packet->UCID, msg->_(packet->UCID, "2101"));
                players[packet->UCID].Sirena = true;
            }
        }
    }

    if (Message == "!radar" || Message == "!^C�����")
    {
        if (players[packet->UCID].cop)
        {
            if (players[packet->UCID].Rank == 1 || players[packet->UCID].Rank == 3)
            {
                insim->SendMTC(UCID, msg->_(UCID, "2111"));
                return;
            }

            if (players[UCID].Info.Speed * 360 / 32768 > 0)
                return;

            if (players[UCID].DTPstatus <= 0)
            {
                if (players[UCID].Radar.On)
                {
                    RadarOff(packet->UCID);
                }
                else
                {
                    RadarOn(packet->UCID);
                }
            }
            else insim->SendMTC(UCID, msg->_(UCID, "2112"));
        }
    }

    if (Message.find("!kick") == 0 )
    {
        if (players[packet->UCID].Rank < 3)
            return;

        if(args.size() < 2)
            return;

        string user = args[1];

        if (players[ packet->UCID ].cop )
        {

            insim->SendMST("/kick " + user);

            CCText(string(players[ packet->UCID ].UName) + " kicked " + string(user));

            tm *timeStruct = tools::GetLocalTime();

            CLog::log(this->ClassName, "kick", StringFormat("%02d:%02d:%02d %s kicked %s", timeStruct->tm_hour, timeStruct->tm_min, timeStruct->tm_sec, players[ packet->UCID ].UName.c_str(), user.c_str()));
        }

    }

    if ( players[ packet->UCID ].Admin)
    {
        if(args.size() < 2)
            return;

        const char* param = args[1].c_str();

        if (Message.find("!cop_add") == 0 )
        {
            if (strlen(param) > 0)
            {
                DB_ROWS res = db->select({"username"}, "police", {{"username",param}});

                if ( res.size() > 0)
                {
                    DB_ROW arFields;
                    arFields["active"] = "Y";

                    db->update("police", arFields, {{"username",param}});
                }
                else
                {
                    char query[MAX_PATH];
                    sprintf(query,"INSERT INTO police (username) VALUES ('%s')",param);
                    db->exec(query);

                }
            }
        }

        if (Message.find("!cop_del") == 0 )
        {
            if (strlen(param) > 0)
            {
                DB_ROW arFields;
                arFields["active"] = "N";

                db->update("police", arFields, {{"username",param}});
            }
        }

        if (Message.find("!cop_up") == 0 )
        {

            for (auto& p: players)
            {
                if (string(players[p.first].UName) == string(param))
                {
                    if (players[p.first].Rank < 4)
                    {
                        players[p.first].Rank++;
                        insim->SendMTC(255, "^2| ^8" + string(players[p.first].PName) + " ^2^C������� �� ^7" + ToString(players[p.first].Rank) + " ^2�����");
                        insim->SendBFNAll(p.first);
                    }
                    else
                        insim->SendMTC(UCID, "^2| ^8" + string(players[p.first].PName) + " ^7^C������ ������������� �����");

                    return;
                }
            }
            insim->SendMTC(UCID, "^2| ^7^C����� �� ������");
        }

        if (Message.find("!cop_down") == 0 )
        {
            for (auto& p: players)
            {
                if (string(players[p.first].UName) == string(param))
                {
                    if (players[p.first].Rank > 1)
                    {
                        players[p.first].Rank--;
                        insim->SendMTC(255, "^2| ^8" + string(players[p.first].PName) + " ^1^C������� �� ^7" + ToString(players[p.first].Rank) + " ^1�����");
                        insim->SendBFNAll(p.first);
                    }
                    else
                        insim->SendMTC(UCID, "^2| ^8" + string(players[p.first].PName) + " ^7^C������ ������������ �����");

                    return;
                }
            }
            insim->SendMTC(UCID, "^2| ^7^C����� �� ������");
        }
    }
}

void RCPolice::RadarOn(byte UCID)
{
    struct streets StreetInfo;
    street->CurentStreetInfo(&StreetInfo, UCID);

    int count = 0;
    for (auto& play: players)
    {
        if (players[play.first].Radar.On && ++count > 2)
        {
            insim->SendMTC(UCID, msg->_(UCID, "EndOfRadar"));
            return;
        }
    }

    if (StreetInfo.SpeedLimit == 0)
    {
        insim->SendMTC(UCID, "^2| ^7�� �� ������ ��������� ����� � ���� �����");
        return;
    }

    int X, Y, H, Z;
    H = players[UCID].Info.Heading / 182;
    X = players[UCID].Info.X / 4096 - cos((H - 60) * M_PI / 180) * 40;
    Y = players[UCID].Info.Y / 4096 - sin((H - 60) * M_PI / 180) * 40;
    Z = players[UCID].Info.Z / 16452;


    ObjectInfo *obj = new ObjectInfo;
    obj->Index = 22;
    obj->Heading = H/360*256;
    obj->X = X;
    obj->Y = Y;
    obj->Zbyte = Z;
    obj->Flags = 0;
    AddObject(obj);

    obj->Index = 138;
    AddObject(obj);
    delete obj;

    players[UCID].Radar.X = X;
    players[UCID].Radar.Y = Y;
    players[UCID].Radar.Z = Z;
    players[UCID].Radar.On = true;

    insim->SendMTC(UCID, msg->_(UCID, "RadarOn"));
}

void RCPolice::RadarOff(byte UCID)
{
    ObjectInfo *obj = new ObjectInfo;
    obj->X = players[UCID].Radar.X;
    obj->Y = players[UCID].Radar.Y;
    obj->Zbyte = players[UCID].Radar.Z;
    obj->Flags = 0;
    obj->Heading = 0;
    obj->Index = 22;
    DelObject(obj);
    obj->Index = 138;
    DelObject(obj);
    delete obj;
    players[UCID].Radar.On = false;
    insim->SendMTC(UCID, msg->_(UCID, "RadarOff"));
}

void
RCPolice::AddBarier(byte UCID,byte copUCID)
{
    int X, Y, H, Z;
    H = players[copUCID].Info.Heading / 182 ;
    X = players[copUCID].Info.X / 4096 - cos((H - 90) * M_PI / 180) * 40 ;
    Y = players[copUCID].Info.Y / 4096 - sin((H - 90) * M_PI / 180) * 40;
    Z = players[copUCID].Info.Z / 16452;

    players[UCID].Barier.X = X;
    players[UCID].Barier.Y = Y;
    players[UCID].Barier.Z = Z;
    players[UCID].Barier.UCID = copUCID;

    H += 90;
    H = (H>360) ? 360-H : H;

    ObjectInfo *obj = new ObjectInfo;
    obj->Index = 104;
    obj->Heading = H * 256 / 360;
    obj->X = X;
    obj->Y = Y;
    obj->Zbyte = Z;
    obj->Flags = 0;
    AddObject(obj);
    delete obj;

    string Msg = StringFormat(msg->_(copUCID, "AddBarier"), players[copUCID].PName.c_str(), players[UCID].PName.c_str());
    insim->SendMTC( copUCID, Msg);
    players[copUCID].Barier.added = true;
}

void
RCPolice::RemoveBarier(byte UCID)
{
    if(players[UCID].Barier.X == 0 && players[UCID].Barier.Y == 0)
        return;

    byte copUCID = players[UCID].Barier.UCID;
    ObjectInfo *obj = new ObjectInfo;
    obj->X = players[UCID].Barier.X;
    obj->Y = players[UCID].Barier.Y;
    obj->Zbyte = players[UCID].Barier.Z;
    obj->Index = 104;
    DelObject(obj);
    delete obj;

    players[UCID].Barier.X = 0;
    players[UCID].Barier.Y = 0;
    players[UCID].Barier.Z = 0;
    players[UCID].Barier.UCID = 0;

    players[ copUCID ].Barier.added = false;
    players[ copUCID ].Barier.canAdd = false;

    insim->SendMTC(copUCID, msg->_(copUCID,"DelBarier"));

}

void RCPolice::CopPayRoll(byte UCID, bool FullWork = true)
{
    if (!players[UCID].cop)
        return;

    int TM = (time(NULL) - players[UCID].StartWork)%COP_WORK_TIME;

    if (FullWork && TM != 0)
        return;

    if (TM < 30 && !FullWork)
        return;

    if (TM == 0 && players[UCID].DoneCount == 0)
    {
        players[UCID].DoneCount = -1;
        insim->SendMTC(UCID, msg->_(UCID,"2113"));
        return;
    }

    if (players[UCID].DoneCount > 10)
        players[UCID].DoneCount = 10;

    if (players[UCID].DoneCount < 0)
        players[UCID].DoneCount = 0;

    if (players[UCID].DoneCount == 0)
        return;

    int Cash = players[UCID].DoneCount * (500 + 250 * players[UCID].Rank);

    string str;

    if (FullWork)
    {
        str = StringFormat("^2| ^7^C��� ��������� �� ��������� �����: %d ^3RUR", Cash);
        players[ UCID ].Barier.canAdd = true;
    }
    else
    {
        Cash = Cash / 2;
        players[UCID].DoneCount = players[UCID].DoneCount / 2;

        str = StringFormat("^2| ^7^C�� �� ��������� ����� �����, ���������: %d ^3RUR", Cash);
    }
    players[ UCID ].Barier.canAdd = false;
    insim->SendMTC(UCID, str);

    bank->RemFrBank(Cash);
    bank->AddCash(UCID, Cash, false);

    if (dl->Islocked(UCID))
    {
        dl->Unlock(UCID);
        dl->AddSkill(UCID, players[UCID].DoneCount);
        dl->Lock(UCID);
    }
    else
    {
        dl->AddSkill(UCID, players[UCID].DoneCount);
    }

    nrg->LockTo(UCID,time(nullptr)+COP_WORK_TIME);

    players[UCID].DoneCount = -1;
}

void RCPolice::ShowFinesPanel( byte UCID, byte UCID2 )
{
    insim->SendBFN(UCID, 79);
    insim->SendBFN(UCID, 80);

    string Text;
    byte c = GetFineCount();

    byte
    id=180, 				//��������� �� ������
    id2=205,
    l=100, 				//�� ������
    t=90,				//�� ������
    hButton=5, 			//������ ������
    w=100, 				//������ ����
    h=10 + c * hButton; 	//������ ����

    Text = StringFormat("^C^7������ ������� ������� (^8%s^7)", players[UCID2].PName.c_str());

    insim->SendButton(255, UCID, 176, l - w / 2, t - h / 2, w, h + 8, 32, ""); 				    //���
    insim->SendButton(255, UCID, 177, l - w / 2, t - h / 2, w, h + 8, 32, "");				    //���
    insim->SendButton(255, UCID, 178, l - w / 2, t - h / 2, w, 10, 64, Text); 				    //���������
    insim->SendButton(254, UCID, 179, l - 7, t - h / 2 + h + 1, 14, 6, 16 + ISB_CLICK, "^2OK"); //����������

    if ( players[UCID].Admin || (players[UCID].Rank > 2 && players[UCID2].Pogonya == 2))
        insim->SendButton(UCID2, UCID, 80, l + w / 2 - 15, t - h / 2 + h + 1, 14, 6, 16 + 8 + 5, "^C����������", 5);
    if(players[UCID2].Info.Speed * 360 / 32768 == 0)
        insim->SendButton(UCID2, UCID, 230,l + w / 2 - 30, t - h / 2 + h + 1, 14, 6, 16 + ISB_CLICK, "^C^2���������");
    if (players[UCID].Rank > 1 && players[UCID2].Pogonya == 1 && players[UCID2].Info.Speed * 360 / 32768 == 0)
        insim->SendButton(UCID2, UCID, 79, l - w / 2 + 1, t - h / 2 + h + 1, 20, 6, 16 + 8 + 5, "^C����� � ������?", 1);

    int i = 0;
    for(xString fineID: jFines.getMemberNames())
    {
        byte fid = fineID.asInt();
        if (fid == 0)
            fid = 255;

        int st = 7;

        if (players[UCID2].Pogonya == 2 || players[UCID].DTPstatus == 2 || players[UCID].Rank == 4)
        {
            for (int i=0; i < MAX_FINES; i++)
                if (FineAllow[players[UCID].Rank][i] == fid)
                {
                    st = ISB_CLICK + ISB_C1 + ISB_C2;
                    break;
                }
        }
        else
        {
            if (fid == 14 || fid == 17 || fid == 20 || fid == 21)
                st = ISB_CLICK + ISB_C1 + ISB_C2;
            if (players[UCID].Rank == 3 && (fid == 16 || fid == 1 || fid == 2))
                st = ISB_CLICK + ISB_C1 + ISB_C2;
        }

        Text = StringFormat("^2%02d. ^8%s", fid, GetFineName(UCID, fineID.asInt()).c_str());

        insim->SendButton(UCID2, UCID, id+fid, l - w / 2 + 1, t - h / 2 + 10 + hButton * i, w - 12, hButton, ISB_LIGHT + ISB_LEFT + st, Text);
        insim->SendButton(UCID2, UCID, id2+fid, l + w / 2 - 11, t - h / 2 + 10 + hButton * i, 10, hButton, ISB_LIGHT + st, "^C������");
        i++;
    }
}

void RCPolice::InsimBTC( struct IS_BTC* packet )
{
    if (packet->ClickID > 100 && packet->ClickID < 110)
    {
        if(players[packet->UCID].cop && !players[packet->UCID].Barier.added)
        {
            AddBarier(packet->ReqI, packet->UCID);
        }
        else if(players[packet->UCID].cop && players[packet->UCID].Barier.added)
        {
            RemoveBarier(packet->ReqI);
        }
    }

    if ( packet->ClickID == 178 && packet->ReqI == 254 )
    {
        players[packet->UCID].ThisFineCount = 0;

        insim->SendBFN(packet->UCID, 176, 239);

        for (int j=0; j < 20; j++)
            strcpy(players[packet->UCID].ThisFine[j], "");

        CopUname = "";
    }

    if ( packet->ClickID == 179 && packet->ReqI == 254 ) //�� � ������ �������
        insim->SendBFN(packet->UCID, 80, 239);

    if (players.find(packet->ReqI) != players.end() && packet->ClickID > 180 && packet->ClickID <= 205 && packet->ReqI != 255) //������� ������
    {
        if(!IsCop(packet->UCID))
            return;

        int FID = packet->ClickID-180;

        if (players[packet->UCID].DTPstatus == 2)
        {
            players[packet->ReqI].blame = true;
            if (FID != 14 && FID != 16 && FID != 17 && FID != 20 && FID != 21)
                players[packet->UCID].FineC += GetFineCash(FID);
        }

        players[packet->UCID].PStat.FinedByDay++;
        players[packet->UCID].PStat.Fined++;

        char Msg[128];
        sprintf(Msg, msg->_(packet->ReqI, "GiveFine"), players[packet->UCID].PName.c_str(), GetFineName(packet->ReqI, FID ).c_str() );
        insim->SendMTC( packet->ReqI, Msg);

        sprintf(Msg, msg->_(packet->UCID , "AddFine" ), players[packet->ReqI].PName.c_str(), GetFineName(packet->UCID, FID ).c_str() );
        insim->SendMTC( packet->UCID , Msg);


        user_fine f;

        f.fine_id = FID;
        f.fine_date = int( time( NULL ) );
        f.CopUName = players[ packet->UCID ].UName;
        f.CopPName = players[ packet->UCID ].PName;
        players[ packet->ReqI ].fines[f.fine_date] = f;


        if (players[packet->ReqI].Pogonya == 2 || players[packet->UCID].DTPstatus == 2)
            if (FID != 14 && FID != 16 && FID != 17 && FID != 20 && FID != 21)
                players[packet->UCID].DTPfines++;

        CLog::log(this->ClassName,"fine",StringFormat("%s add fine ID = %d for %s", players[ packet->UCID ].UName.c_str(), FID, players[ packet->ReqI ].UName.c_str()));


        if (players[packet->ReqI].ThisFineCount == 0)
            insim->SendBFNAll(packet->ReqI);

        for (int j=0; j < 20; j++)
        {
            if ( strlen(players[packet->ReqI].ThisFine[j]) == 0 )
            {
                sprintf(players[packet->ReqI].ThisFine[j], "^7%d. %s (^2ID = %d^7)   -   %s", j + 1, GetFineName(packet->ReqI, FID).c_str(), FID, players[ packet->UCID ].PName.c_str());
                players[packet->ReqI].ThisFineCount++;
                break;
            }
        }
    }

    if (players.find(packet->ReqI) != players.end() && packet->ClickID > 205 && packet->ClickID <= 239 && packet->ReqI != 255) //������ ������
    {
        int FID = packet->ClickID-205;
        if (players[packet->UCID].DTPstatus == 2)
            if (FID != 14 && FID != 16 && FID != 17 && FID != 20 && FID != 21)
                players[packet->UCID].FineC -= GetFineCash(FID);

        for (auto f = players[ packet->ReqI ].fines.begin(); f != players[ packet->ReqI ].fines.end(); ++f)
        {
            if ( f->second.fine_id == FID )
            {
                players[packet->UCID].PStat.CanceledFinesByDay++;
                players[packet->UCID].PStat.CanceledFines++;

                if (players[packet->ReqI].Pogonya == 2 || players[packet->UCID].DTPstatus == 2)
                    players[packet->UCID].DTPfines--;

                char Msg[128];
                sprintf(Msg, msg->_( packet->ReqI, "DeletedFine"), players[packet->UCID].PName.c_str(), GetFineName(packet->ReqI, FID).c_str());
                insim->SendMTC( packet->ReqI , Msg);

                sprintf(Msg, msg->_(packet->UCID, "DelFine"), players[ packet->ReqI ].PName.c_str(), GetFineName(packet->UCID, FID).c_str());
                insim->SendMTC( packet->UCID , Msg);

                players[ packet->ReqI ].fines.erase(f->second.fine_date);

                CLog::log(this->ClassName,"fine",StringFormat("%s cancel fine ID = %d for %s", players[ packet->UCID ].UName.c_str(), FID, players[ packet->ReqI ].UName.c_str()));

                if (players[packet->ReqI].ThisFineCount > 0)
                {
                    for (int j=0; j < 20; j++)
                    {
                        if ( strlen(players[packet->ReqI].ThisFine[j]) == 0 )
                        {
                            sprintf(players[packet->ReqI].ThisFine[j], "   ^1^K�� ^7%s (^2ID = %d^7)   -   %s", GetFineName(packet->ReqI, FID).c_str(), FID, players[ packet->UCID ].PName.c_str());
                            players[packet->ReqI].ThisFineCount++;
                            break;
                        }
                    }
                }
                break;
            }
        }
    }

    /** ������ **/
    if (packet->ClickID == 73 && players[packet->UCID].cop)
        ShowFinesPanel(packet->UCID, packet->ReqI);

    /** �������� ������ **/
    if (packet->ClickID == 74)
    {
        if ( players[packet->ReqI].UName.length() == 0 )
            return;

        bool finded = false;
        for(PLIDMap::iterator it = PLIDtoUCID.begin(); it != PLIDtoUCID.end(); ++it)
        {
            if( packet->ReqI == it->second)
                finded = true;
        }

        if(!finded)
            return;

        if (players[packet->UCID].DTPstatus <= 0)
        {
            if (players[packet->ReqI].Pogonya == 0)
            {
                players[packet->UCID].FineC = 0;
                players[packet->UCID].DTPfines = 0;
                //players[packet->UCID].DTPstatus = -1;
                players[packet->ReqI].Pogonya = 1;
                int worktime = time(NULL);
                players[packet->ReqI].WorkTime = worktime + 60 * 5;

                insim->SendMTC(255, StringFormat(msg->_(packet->ReqI, "PogonyaOn" ), players[packet->ReqI].PName.c_str(), players[packet->UCID].PName.c_str()));
                nrg->LockTo(packet->ReqI,worktime + 60 * 5);
            }
        }
        else
        {
            insim->SendMTC(packet->UCID, msg->_(packet->UCID,"2119"));
        }

        return;
    }


    /** ��������� ������ **/
    if (packet->ClickID == 75)
    {

        if ( players[packet->ReqI].UName.length() == 0)
            return;

        if (players[packet->ReqI].Pogonya != 0)
        {
            RemoveBarier(packet->ReqI);
            players[packet->UCID].FineC = 0;

            //��������� �����, ������� ���� ����� �� ����� ������
            if (players[packet->UCID].DTPfines > 0)
            {
                players[packet->UCID].PStat.ArrestWithFineByDay++;
                players[packet->UCID].PStat.ArrestWithFine++;
                players[packet->UCID].DoneCount++;
                players[packet->UCID].DTPfines = 0;
                for (auto& p: players)
                {
                    if (players[p.first].cop && p.first != packet->UCID && players[p.first].Rank > 1)
                    {
                        int X1 = players[packet->ReqI].Info.X / 65536,
                            Y1 = players[packet->ReqI].Info.Y / 65536,
                            X2 = players[p.first].Info.X / 65536,
                            Y2 = players[p.first].Info.Y / 65536,
                            Dist2 = Distance(X1, Y1, X2, Y2);

                        if (Dist2 < 30)
                        {
                            insim->SendMTC(p.first, "^2| ^7^C�� �������� �������� � �������� �� ���������� ��� ������");

                            players[p.first].PStat.HelpArrest++;
                            players[p.first].PStat.HelpArrestByDay ++;

                            players[p.first].DoneCount++;
                            players[p.first].DTPfines = 0;
                        }
                    }
                }
            }
            else
            {
                players[packet->UCID].PStat.ArrestWithOutFineByDay++;
                players[packet->UCID].PStat.ArrestWithOutFine++;
            }

            players[packet->UCID].DTPstatus = 0;
            for (int i = 0; i < 32; i++)
            {
                if (DTPvyzov[0][i] != 0 && DTPvyzov[2][i] == packet->UCID)
                {
                    players[packet->UCID].DTPstatus = 1;
                    break;
                }
            }

            players[packet->ReqI].Pogonya = 0;

            insim->SendMTC(255, StringFormat(msg->_(packet->ReqI, "PogonyaOff" ), players[packet->ReqI].PName.c_str(), players[packet->UCID].PName.c_str()));
            nrg->LockTo(packet->ReqI);
            ClearButtonClock(packet->ReqI);

            //���������� �������
            insim->SendBFN(packet->UCID, 81, 165);

            //������� ������
            insim->SendBFN(255, 101, 110);
        }
        return;
    }
}

void RCPolice::InsimBTT( struct IS_BTT* packet )
{

    /** ������� �� ������ */
    if (packet->ReqI == 200)
    {
        int i = packet->ClickID - 91;

        if (players[packet->UCID].DTPstatus == 2 && DTPvyzov[2][i] == packet->UCID)
        {
            insim->SendMTC(DTPvyzov[0][i], StringFormat(msg->_(DTPvyzov[0][i],"2114"), players[packet->UCID].PName.c_str()));

            insim->SendMTC(packet->UCID, StringFormat(msg->_(packet->UCID,"2115"), players[DTPvyzov[0][i]].PName.c_str()));

            players[DTPvyzov[0][i]].DTP = 0;
            if (DTPvyzov[2][i] != 0)
                players[DTPvyzov[2][i]].DTPstatus = 0;

            if (players[packet->UCID].DTPfines > 0)
            {
                players[packet->UCID].DoneCount++;
                players[packet->UCID].PStat.SolvedIncedentsByDay++;
                players[packet->UCID].PStat.SolvedIncedents++;
            }
            players[packet->UCID].DTPfines = 0;

            //������� ����������� � ���� % �� �������
            if (!players[DTPvyzov[0][i]].blame && players[packet->UCID].FineC>0)
            {
                insim->SendMTC(DTPvyzov[0][i], StringFormat(msg->_(DTPvyzov[0][i],"Compens"), (double)((double)players[packet->UCID].FineC*0.05)));
                insim->SendMTC(packet->UCID, StringFormat(msg->_(packet->UCID,"CompensPl"), players[DTPvyzov[0][i]].PName.c_str(), (double)((double)players[packet->UCID].FineC*0.05)));
                bank->RemFrBank(players[packet->UCID].FineC*0.05);
                bank->AddCash(DTPvyzov[0][i], (players[packet->UCID].FineC*0.05), true);


                CLog::log(this->ClassName,"compensation",StringFormat("%s get compensation %0.0f from %s", players[DTPvyzov[0][i]].UName.c_str(), (double)((double)players[packet->UCID].FineC*0.05), players[packet->UCID].UName.c_str()));
            }

            players[packet->UCID].FineC = 0;
            players[DTPvyzov[0][i]].blame = false;

            DTPvyzov[0][i] = 0;
            DTPvyzov[1][i] = 0;
            DTPvyzov[2][i] = 0;
            insim->SendBFN(255, packet->ClickID);

            //���������� �������
            insim->SendBFN(packet->UCID, 80, 239);

            //������� ������
            insim->SendBFN(255, 80, 110);
        }
        else if (DTPvyzov[2][i] <= 0)
        {
            if (players[packet->UCID].DTPstatus > 0)
            {
                insim->SendMTC(packet->UCID, msg->_(packet->UCID,"2116"));
                return;
            }

            DTPvyzov[2][i] = packet->UCID;
            DTPvyzov[1][i] = 0;

            insim->SendMTC(DTPvyzov[0][i], StringFormat(msg->_(DTPvyzov[0][i],"2117"), players[packet->UCID].PName.c_str()));

            SendMTCToCop(StringFormat(msg->_(packet->UCID,"2118"), players[packet->UCID].PName.c_str(), players[DTPvyzov[0][i]].PName.c_str()), 1, 2, 4);

            players[DTPvyzov[2][i]].DTPstatus = 1;
            players[DTPvyzov[2][i]].DTPfines = 0;
            players[DTPvyzov[2][i]].FineC = 0;
            players[DTPvyzov[0][i]].DTP = packet->UCID;
        }
    }

    /** ����� � ������ ����� � ������ **/
    if ( packet->ClickID == 79)
    {
        players[packet->ReqI].Pogonya = 2;
        ShowFinesPanel(packet->UCID, packet->ReqI);
        insim->SendBFN(packet->UCID, 79);

        insim->SendMTC(packet->ReqI, "^2| ^C^7���� � ������ �� ����� ������");
        insim->SendMTC(255, StringFormat("^2| %s%s", players[packet->ReqI].PName.c_str(), msg->_(packet->ReqI, "1702")));
        insim->SendMTC(packet->UCID, msg->_(packet->UCID, "1703"));
    }

    /** ����� �� � ����� **/
    if (players.find(packet->ReqI) != players.end() && packet->ClickID == 80)
    {
        if (strlen(packet->Text) == 0)
            return;

        players[packet->ReqI].Pogonya = 0;
        nrg->LockTo(packet->ReqI);

        //���������� �������
        insim->SendBFN(packet->UCID, 81, 165);

        //������� ������
        insim->SendBFN(255, 101, 110);

        time_t now = time(NULL);

        int MAXmin = 0;
        if (players[packet->UCID].Rank == 3)
            MAXmin = 30;

        if (players[packet->UCID].Rank == 4)
            MAXmin = 60;

        if(players[packet->UCID].Admin)
            MAXmin = 60*24*7;

        int Min = atoi(packet->Text);
        if (Min > MAXmin)
            Min = MAXmin;
        else if (Min < 5 && Min > 0)
            Min = 5;

        if (Min > 0)
        {
            ArestPlayers[ players[packet->ReqI].UName ] = now + Min * 60;

            insim->SendMST("/spec " + players[packet->ReqI].UName);

            insim->SendMTC(255, StringFormat(msg->_(packet->ReqI, "ArestedMin"), players[packet->ReqI].PName.c_str(), Min));
        }
        else if ( ArestPlayers.find(  players[packet->ReqI].UName ) != ArestPlayers.end() )
        {
            ArestPlayers.erase(  players[packet->ReqI].UName );

            insim->SendMTC(255, StringFormat(msg->_(packet->ReqI, "Reliaved"), players[packet->ReqI].PName.c_str()));
            ClearButtonClock(packet->ReqI);
            insim->SendBFN(packet->ReqI, 86);
        }
    }
}

void RCPolice::InsimPEN( struct IS_PEN* packet )
{
    byte UCID = PLIDtoUCID[ packet->PLID ];

    if (packet->Reason == PENR_WRONG_WAY )
    {
        user_fine f;
        f.fine_id = 18;
        f.fine_date = int(time(NULL));
        players[ UCID ].fines[f.fine_date] = f;

        insim->SendMTC( UCID , StringFormat( msg->_( UCID , "GiveFine2" ), GetFineName(UCID, 18).c_str()));

    }
    else if (packet->Reason == PENR_SPEEDING )
    {

        if ( packet->NewPen == PENALTY_30 ||
                packet->NewPen == PENALTY_45 ||
                packet->NewPen == PENALTY_DT ||
                packet->NewPen == PENALTY_SG )
        {
            user_fine f;
            f.fine_id = 13;
            f.fine_date = int(time(NULL));
            players[ UCID ].fines[f.fine_date] = f;

            insim->SendMTC( UCID , StringFormat( msg->_( UCID , "GiveFine2" ), GetFineName(UCID, 13).c_str()));
        }
    }
}

void RCPolice::InsimOBH( struct IS_OBH* packet )
{
    if (packet->Index == 22 || packet->Index == 138)
    {
        for (auto& play: players)
        {
            byte UCID = play.first;

            if (players[UCID].Radar.On && players[UCID].Radar.X == packet->X && players[UCID].Radar.Y == packet->Y)
            {
                insim->SendMTC(UCID, "^2| ^C^7����� ^1������");
                RadarOff(UCID);
            }
        }
    }
}

void RCPolice::InsimPLA( struct IS_PLA* packet )
{
    byte UCID = PLIDtoUCID[ packet->PLID ];

    if (packet->Fact == PITLANE_EXIT)
    {

        insim->SendMST("/p_clear " + players[ UCID ].UName);

        if (players[ UCID ].fines.size() > 10)
        {
            insim->SendMTC( UCID , msg->_(  UCID , "3400" ));
            insim->SendMST("/pitlane " + players[ UCID ].UName);
        }
    }
}

void RCPolice::InsimMCI( struct IS_MCI* packet )
{
    for (int i = 0; i < packet->NumC; i++)
    {
        byte UCID = PLIDtoUCID[packet->Info[i].PLID];

        if (UCID == 0)
            return;

        int SirenaCount = 0;
        int SDtemp=0;

        int S = packet->Info[i].Speed * 360 / 32768;

        /** ������ �� �������� **/
        if ( players[UCID].ThisFineCount != 0 )
        {
            byte ClickID=180, w=90, h=10 + 5 * players[UCID].ThisFineCount, l=100, t=90;
            insim->SendButton(255, UCID, 176, l - w / 2, t - h / 2, w, h + 8, 32, "");
            insim->SendButton(255, UCID, 177, l - w / 2, t - h / 2, w, h + 8, 32, "");									//���
            insim->SendButton(254, UCID, 178, l - 7, t - h / 2 + h + 1, 14, 6, 16 + 8, "^2OK"); 		    //����������
            insim->SendButton(255, UCID, 179, l - w / 2, t - h / 2, w, 10, 3 + 64, msg->_(UCID, "GiveFine3")); 			//���������

            for (int j=0; j < players[UCID].ThisFineCount; j++)
                insim->SendButton(255, UCID, ClickID++, l - w / 2 + 1, t - h / 2 + 10 + 5 * j, w - 2, 5, ISB_LEFT + ISB_LIGHT, players[UCID].ThisFine[j]);
        }

        /** �������������� ������ **/
        if (players[UCID].Radar.On && Distance(players[UCID].Radar.X, players[UCID].Radar.Y, players[UCID].Info.X/4096, players[UCID].Info.Y/4096) > 150)
        {
            RadarOff(UCID);
        }

        /** �������������� ������� **/
        byte copUCID = players[UCID].Barier.UCID;
        if (copUCID != 0 && players[copUCID].Barier.added && Distance(players[UCID].Barier.X, players[UCID].Barier.Y, players[copUCID].Info.X/4096, players[copUCID].Info.Y/4096) > 150)
        {
            RemoveBarier(UCID);
        }

        /** ������ ���� **/
        if (players[UCID].cop && players[UCID].Sirena)
            insim->SendButton(255, UCID, 81, 90, 26, 20, 10, 32, siren.c_str());
        else if (players[UCID].cop)
            insim->SendBFN(UCID, 81);

        for (auto& play: players)
        {
            if (UCID == play.first)
            {
                continue;
            }

            int X1 = players[UCID].Info.X / 65536,
                Y1 = players[UCID].Info.Y / 65536,
                X2 = players[play.first].Info.X / 65536,
                Y2 = players[play.first].Info.Y / 65536,
                Dist = Distance(X1, Y1, X2, Y2);

            /** ����� **/
            if (players[UCID].Pogonya == 1 && players[play.first].cop && players[play.first].Rank != 1)
            {
                if (S < 5 && Dist < 10)
                {
                    players[UCID].StopTime++;
                    if (players[UCID].StopTime >= 4)
                    {
                        players[UCID].StopTime = 0;
                        players[UCID].Pogonya = 2;

                        insim->SendMTC(255, StringFormat("^2| %s%s", players[UCID].PName.c_str(), msg->_(UCID, "1702")));
                        insim->SendMTC(play.first, msg->_(play.first, "1703"));
                        RemoveBarier(UCID);
                    }
                }
                if (S > 5)
                    players[UCID].StopTime = 0;
            }

            /** ������ � ������ **/
            if (players[play.first].Sirena && players[play.first].cop && Dist < 180)
            {
                SirenaCount++;
                if (Dist < SDtemp || SDtemp ==0)
                    SDtemp = Dist;
            }
            players[UCID].SirenaDist = SDtemp;

            /** ������ �� ��� **/
            if (players[UCID].DTP != 0 && play.first == players[UCID].DTP && Dist < 30 && players[play.first].DTPstatus == 1 && players[play.first].Info.Speed * 360 / 32768 < 5)
            {
                players[play.first].DTPstatus = 2;
                players[UCID].DTP = 0;
            }

            /** ����� **/
            if (!players[UCID].cop && players[UCID].Pogonya == 0 && players[play.first].cop && players[play.first].Radar.On)
            {
                Dist = Distance(X1, Y1, players[play.first].Radar.X/16, players[play.first].Radar.Y/16);

                if (Dist < 100)
                {
                    struct streets StreetInfo;
                    street->CurentStreetInfo(&StreetInfo, UCID);

                    if (Dist < 50)
                    {
                        int Speed = players[UCID].Info.Speed * 360 / 32768;


                        if (Speed > StreetInfo.SpeedLimit + 20 && Speed > players[UCID].speed_over)
                            players[UCID].speed_over = Speed - StreetInfo.SpeedLimit;

                    }
                    else if (players[UCID].speed_over > 0)
                    {
                        string text;

                        text = StringFormat(msg->_(UCID, "Speeding"), players[UCID].PName.c_str(), players[UCID].speed_over, street->GetStreetName(UCID, StreetInfo.StreetID), players[play.first].PName.c_str());
                        insim->SendMTC(255, text);

                        if (players[UCID].Pogonya == 0)
                        {
                            players[UCID].Pogonya = 1;
                            int worktime = time(NULL);
                            players[UCID].WorkTime = worktime + 60 * 5;
                            text = StringFormat(msg->_(UCID, "PogonyaOn" ), players[UCID].PName.c_str(), players[play.first].PName.c_str() );
                            insim->SendMTC(255, text);
                            nrg->LockTo( UCID, worktime + 60 * 5);
                        }
                        players[UCID].speed_over=0;
                    }
                }
                else if (players[UCID].speed_over>0)
                    players[UCID].speed_over=0;
            }
        }

        /** ������ � ������ **/
        if (SirenaCount > 0)
        {
            if (players[UCID].cop)
                insim->SendButton(255, UCID, 82, 0, 36, 200, (byte)((180 - players[UCID].SirenaDist + 4) / 10)+1, 0, siren.c_str());
            else
                insim->SendButton(255, UCID, 82, 0, 36, 200, (180 - players[UCID].SirenaDist + 4) / 4, 0, siren.c_str());
        }
        else if (players[UCID].SirenaDist > 0 || SirenaCount == 0)
        {
            players[UCID].SirenaDist = 0;
            insim->SendBFN(UCID, 82);
        }

        if (players[UCID].Pogonya == 0)
        {
            insim->SendBFN(UCID, 83);
            insim->SendBFN(UCID, 84);
            //insim->SendBFN(UCID, 85);
        }

        if (players[UCID].Pogonya != 0)
        {
            BtnPogonya(UCID);
        }

        players[UCID].Info = packet->Info[i];
    }
}

void RCPolice::ReadConfig(const char* Track)
{
    ReadFines();

    char file[MAX_PATH];
    sprintf(file, "%s/RCPolice/FineAllow.txt", RootDir);

    ifstream readf (file, ios::in);

    if (!readf.is_open())
    {
        CCText("  ^7RCPolice   ^1ERROR: ^8file " + string(file) + " not found");
        return;
    }

    while (readf.good())
    {
        char line[128];
        readf.getline(line, 128);

        if(strlen(line) <= 1)
            continue;

        char str[128];

        char* ch = strtok (line, ";");
        if(ch != nullptr)
        {
            strcpy(str, ch);

            if (strlen(str) > 1)
            {
                char* crank = strtok(str, "=");

                if(crank != nullptr)
                {
                    int rnk = atoi(crank);

                    if (rnk == 0 || rnk > 4)
                        continue;

                    for (int i=0; i < MAX_FINES; i++)
                    {
                        char* cc = strtok(NULL, ",");
                        int c = 0;
                        if( cc != nullptr )
                            c = atoi(cc);

                        if (c == 0)
                            break;

                        FineAllow[rnk][i] = c;
                    }
                }

            }
        }


    }
    readf.close();

    CCText("  ^7RCPolice\t^2OK");
}

void RCPolice::SetSirenLight( string sirenWord )
{
    siren = sirenWord;
}

bool RCPolice::IsCop( byte UCID )
{
    return players[UCID].cop;
}

int RCPolice::GetCopRank( byte UCID )
{
    return players[UCID].Rank;
}

int RCPolice::GetCopDTPstatus( byte UCID )
{
    return players[UCID].DTPstatus;
}

int RCPolice::InPursuite( byte UCID )
{
    return players[UCID].Pogonya;
}

int RCPolice::GetCopCount()
{
    int c = 0;
    for ( auto& play: players )
        if (players[play.first].cop && players[play.first].Rank !=3)
            c++;
    return c;
}

int RCPolice::GetFineCount()
{
    return jFines.getMemberNames().size();
}

void RCPolice::BtnPogonya(byte UCID)
{
    if (players[UCID].Pogonya == 1)
    {
        insim->SendButton(255, UCID, 83, 0, 20, 200, 30, 1, msg->_(UCID, "RideButton" ));
        insim->SendButton(255, UCID, 84, 0, 43, 200, 6, 0, msg->_(UCID, "RightAndStop" ));
    }
    else if (players[UCID].Pogonya == 2)
    {
        insim->SendButton(255, UCID, 83, 0, 20, 200, 30, 1, msg->_(UCID, "ArestButton"));
        insim->SendBFN(UCID, 84);
    }
}

void RCPolice::Save ( byte UCID )
{
    if (players[UCID].cop)
        SaveCopStat(UCID);

    db->exec("DELETE FROM fines WHERE username = '"+players[ UCID ].UName+"'");

    for (auto f: players[ UCID ].fines)
    {
        DB_ROW row;
        row["username"] = players[ UCID ].UName;
        row["fine_id"] = ToString(f.second.fine_id);
        row["fine_date"] = ToString(f.second.fine_date);
        row["copUName"] = f.second.CopUName;
        row["copPName"] = f.second.CopPName;

        db->insert("fines", row);
    }
}

void RCPolice::ReadUserFines( byte UCID )
{

    DB_ROWS result = db->select({"fine_id", "fine_date", "copUName","copPName"}, "fines", {{"username",players[ UCID ].UName}});

    if(result.size() > 0)
    {
        for (auto row: result )
        {
            user_fine f;

            f.fine_id = row["fine_id"].asInt();
            f.fine_date = row["fine_date"].asInt();
            f.CopUName = row["copUName"];
            f.CopPName = row["copPName"];

            players[UCID].fines[f.fine_date] = f;
        }
    }
}

bool
RCPolice::isValidPrefix(xString PName)
{
    return (PName.find("^4[^C^7���^4]") != string::npos || PName.find("^4[^C^7���^4]") != string::npos || PName.find("^4[^7COP^4]") != string::npos);
}

void RCPolice::ReadFines()
{
    char file[255];
    strcpy(file, RootDir);
    sprintf(file, "%s/RCPolice/fines.json" , RootDir);

    ifstream readf (file, ios::in);

    if (!readf.is_open())
    {
        cout << "Can't find " << file << endl;
        return;
    }

    bool readed = configReader.parse( readf, jFines, false );

    readf.close();
    if ( !readed )
	{
		// report to the user the failure and their locations in the document.
		cout  << "Failed to parse configuration\n"
				   << configReader.getFormattedErrorMessages();
		return;
	}
}

string RCPolice::GetFineName(byte UCID, int FineID)
{
    string sFineID = StringFormat("%02d",FineID);

    if(!jFines.isMember(sFineID))
        return "";

    return tools::convert_encoding(jFines[sFineID]["lang"][ msg->GetLang(UCID) ].asString(),"UTF-8","CP1251");
}

int
RCPolice::GetFineCash(int FineID)
{
    string sFineID = StringFormat("%02d",FineID);

    if(!jFines.isMember(sFineID))
        return -1;

    return jFines[sFineID]["cash"].asInt();
}

void RCPolice::SendMTCToCop(string Msg, int Rank, ...)
{
    int *p=&Rank;
    while (*p)
        for ( auto& play: players )
            if (players[play.first].cop && players[play.first].Rank == *p++)
                insim->SendMTC(play.first, Msg);
}

bool
RCPolice::LoadCopStat(byte UCID)
{
    DB_ROWS res = db->select({}, "police", {{"username",players[UCID].UName}});

    if ( res.size() > 0)
    {
        DB_ROW row = res.front();

        if (row["active"] == "N")
        {
            return false;
        }

        players[UCID].Rank = row["rank"].asInt();

        players[UCID].PStat.DateActive = row["date_active"].asInt();
        players[UCID].PStat.CurrentDay = row["current_day"].asInt();
        players[UCID].PStat.ArrestWithFineByDay = row["arrests_with_fine_by_day"].asInt();
        players[UCID].PStat.ArrestWithOutFineByDay = row["arrests_without_fine_by_day"].asInt();
        players[UCID].PStat.HelpArrestByDay = row["helparrest_by_day"].asInt();
        players[UCID].PStat.SolvedIncedentsByDay = row["solved_accidents_by_day"].asInt();
        players[UCID].PStat.FinedByDay = row["fined_by_day"].asInt();
        players[UCID].PStat.CanceledFinesByDay = row["fines_canceled_by_day"].asInt();
        players[UCID].PStat.ArrestWithFine = row["arrests_with_fine"].asInt();
        players[UCID].PStat.ArrestWithOutFine = row["arrests_without_fine"].asInt();
        players[UCID].PStat.HelpArrest = row["helparrest"].asInt();
        players[UCID].PStat.SolvedIncedents = row["solved_accidents"].asInt();
        players[UCID].PStat.Fined = row["fined"].asInt();
        players[UCID].PStat.CanceledFines = row["fines_canceled"].asInt();

    }
    else
        return false;

    return true;
}

void RCPolice::SaveCopStat(byte UCID)
{

    DB_ROW arFields;
    arFields["rank"] = ToString(players[UCID].Rank);
    arFields["date_active"] = ToString(players[UCID].PStat.DateActive);
    arFields["current_day"] = ToString(players[UCID].PStat.CurrentDay);
    arFields["arrests_with_fine_by_day"] = ToString(players[UCID].PStat.ArrestWithFineByDay);
    arFields["arrests_without_fine_by_day"] = ToString(players[UCID].PStat.ArrestWithOutFineByDay);
    arFields["helparrest_by_day"] = ToString(players[UCID].PStat.HelpArrestByDay);
    arFields["solved_accidents_by_day"] = ToString(players[UCID].PStat.SolvedIncedentsByDay);
    arFields["fined_by_day"] = ToString(players[UCID].PStat.FinedByDay);
    arFields["fines_canceled_by_day"] = ToString(players[UCID].PStat.CanceledFinesByDay);
    arFields["arrests_with_fine"] = ToString(players[UCID].PStat.ArrestWithFine);
    arFields["arrests_without_fine"] = ToString(players[UCID].PStat.ArrestWithOutFine);
    arFields["helparrest"] = ToString(players[UCID].PStat.HelpArrest);
    arFields["solved_accidents"] = ToString(players[UCID].PStat.SolvedIncedents);
    arFields["fined"] = ToString(players[UCID].PStat.Fined);
    arFields["fines_canceled"] = ToString(players[UCID].PStat.CanceledFines);

    bool up = db->update("police", arFields, {{"username",players[UCID].UName}});

    if ( !up )
    {
        CCText("^1RCPolice: " + (string)players[UCID].UName + " SaveStat error");
        insim->SendMTC(255, "^1RCPolice: " + (string)players[UCID].UName + " SaveStat error");
        return;
    }
}

void RCPolice::Event()
{
    int nowtime = time( NULL );

    lgh->allowPedestrians = true;
    for ( auto& play: players )
    {
        byte UCID = play.first;
        auto playr = play.second;

        // ��������� ������������ �������
        if( ArestPlayers.find( playr.UName ) != ArestPlayers.end() )
        {
            if ( nowtime > ArestPlayers[ playr.UName ] )
            {
                ArestPlayers.erase( playr.UName );

                insim->SendMTC(255, StringFormat(msg->_(UCID, "Reliaved"), playr.UName.c_str()));
                ClearButtonClock(UCID);
                insim->SendBFN(UCID, 86);
            }
            else
            {
                ButtonClock(UCID, ArestPlayers[ playr.UName ] - nowtime);
                insim->SendButton(255, UCID, 86, 120, 9, 20, 5, 32, msg->_(UCID, "YouUndArest"));
            }
        }

        if ( playr.Pogonya == 1 )
        {
            lgh->allowPedestrians = false;


            ButtonClock(UCID, players[UCID].WorkTime - nowtime);
            if (playr.WorkTime <= nowtime)
            {
                ClearButtonClock(UCID);

                insim->SendMST(StringFormat("/msg ^2| %s %s" , playr.PName.c_str(), msg->_(  UCID , "1706" )));
                players[ UCID ].Pogonya = 0;
                nrg->LockTo(UCID);
                dl->AddSkill(UCID);
                ClearButtonClock(UCID);

                //������� ������
                for (byte i = 101; i < 110; i++)
                    insim->SendBFN(255, i);
            }
        }

        if (players[UCID].cop)
        {
            streets StreetInfo;

            CopPayRoll(UCID);

            char smn[32];
            int TM = 1800 - (nowtime - players[UCID].StartWork)%1800;
            sprintf(smn, "^C�����: %02d:%02d", (TM / 60)%60, TM%60);
            insim->SendButton(255, UCID, 87, 115, 1, 15, 4, 3 + 128, smn);

            //������ �� ���
            for (int i = 0; i < 32; i++)
            {
                if (DTPvyzov[0][i] >= 0 && DTPvyzov[1][i] >= 0 && players[UCID].DTPstatus != -1 && players[UCID].Rank != 3)
                {
                    street->CurentStreetInfo(&StreetInfo, DTPvyzov[0][i]);	// �����, ��� ���
                    byte id = 91;											// ��������� �� ������

                    if (DTPvyzov[2][i] <= 0 && DTPvyzov[1][i] > 0)
                    {
                        int T = DTPvyzov[1][i] - nowtime; 				// ������� �������� �� ����� ������

                        string str = StringFormat("^C^1����� �� ��� - ^8%s^7, %s, ^1%02d:%02d", players[DTPvyzov[0][i]].PName.c_str(), msg->_(UCID, StreetInfo.StreetName), (T / 60)%60, T%60);
                        insim->SendButton(200, UCID, id + i, 159, 100 + 4 * i, 40, 4, 32 + 8, "\0^C��������� �������\0" + string(str), 1);

                        if (T <= 0)
                        {
                            players[DTPvyzov[0][i]].DTP = 0;

                            if (DTPvyzov[2][i] != 0)
                                players[DTPvyzov[2][i]].DTPstatus = 0;

                            DTPvyzov[0][i] = 0;
                            DTPvyzov[1][i] = 0;
                            DTPvyzov[2][i] = 0;
                            insim->SendBFN(255, id + i);

                            //����� ���� ����� ��� ��������� ������
                            for (auto& play: players)
                            {
                                if (players[play.first].cop && players[play.first].Rank != 3 && players[play.first].DTPstatus == 0)
                                {
                                    insim->SendMTC(play.first, msg->_(play.first,"2120"));
                                    if (dl->Islocked(play.first))
                                    {
                                        dl->Unlock( play.first );
                                        dl->RemSkill( play.first, 0.2);
                                        dl->Lock( play.first );
                                    }
                                    else
                                    {
                                        dl->RemSkill( play.first, 0.2);
                                    }
                                }
                            }
                        }
                    }
                    else if (DTPvyzov[2][i] == UCID)
                    {
                        if (players[UCID].DTPstatus == 1)
                        {
                            string str = StringFormat(msg->_(UCID,"2121"), players[DTPvyzov[0][i]].PName.c_str(), msg->_(UCID, StreetInfo.StreetName));
                            insim->SendButton(200, UCID, id + i, 159, 100 + 4 * i, 40, 4, 32 + 3, str);
                        }
                        else
                        {
                            string str = StringFormat(msg->_(UCID,"2122"), players[DTPvyzov[0][i]].PName.c_str(), msg->_(UCID, StreetInfo.StreetName));
                            insim->SendButton(200, UCID, id + i, 159, 100 + 4 * i, 40, 4, 32 + 8, "\0^C��������� �������\0" + string(str), 1);
                        }
                    }
                    else if (DTPvyzov[0][i] > 0 && DTPvyzov[2][i] > 0)
                    {
                        string str = StringFormat(msg->_(UCID,"2123"), players[DTPvyzov[0][i]].PName.c_str(), msg->_(UCID, StreetInfo.StreetName));
                        insim->SendButton(200, UCID, id + i, 159, 100 + 4 * i, 40, 4, 32 + 7, str);
                    }
                }
            }

            /** ������ */
            int id = 111,
                L = 58,
                H = 1;

            insim->SendButton(255, UCID, id++, L, H + 3, 6, 2, 0, "^3^K��");
            insim->SendButton(255, UCID, id++, L - 1, H - 1, 5, 10, 0, "^1^J�s");
            insim->SendButton(255, UCID, id++, L - 1, H - 1, 14, 10, 0, "^1^J             �b");
            insim->SendButton(255, UCID, id++, L - 0, H - 1, 12, 10, 0, "^1^J�P�P�P�P");
            insim->SendButton(255, UCID, id++, L - 0, H - 1, 12, 10, 0, "^1^J�Q�Q�Q�Q");

            if (players[UCID].Rank == 1)
            {
                //��. ����.
                insim->SendButton(255, UCID, id++, L + 6, H + 2, 4, 4, 0, "^3^J��");
            }
            else if (players[UCID].Rank == 2)
            {
                //����
                insim->SendButton(255, UCID, id++, L + 6, H + 1, 5, 3, 0, "^3^J��");
                insim->SendButton(255, UCID, id++, L + 6, H + 4, 5, 3, 0, "^3^J��");
            }
            else if (players[UCID].Rank == 3)
            {
                //��. ����
                insim->SendButton(255, UCID, id++, L + 4, H + 2, 5, 4, 0, "^3^J��");
                insim->SendButton(255, UCID, id++, L + 6, H + 1, 5, 3, 0, "^3^J��");
                insim->SendButton(255, UCID, id++, L + 6, H + 4, 5, 3, 0, "^3^J��");
            }
            else if (players[UCID].Rank == 4)
            {
                //�������
                insim->SendButton(255, UCID, id++, L + 2, H + 2, 6, 4, 0, "^3^J��");
                insim->SendButton(255, UCID, id++, L + 5, H + 2, 5, 4, 0, "^3^J��");
                insim->SendButton(255, UCID, id++, L + 7, H + 1, 5, 3, 0, "^3^J��");
                insim->SendButton(255, UCID, id++, L + 7, H + 4, 5, 3, 0, "^3^J��");
            }


            /** ������ ������ **/

            id = 101;
            byte T = 194;
            for ( auto& play2: players)
            {
                byte UCID2 = play2.first;
                auto playr2 = play2.second;
                if ( playr2.Pogonya != 0 )
                {
                    int time2 = playr2.WorkTime - nowtime;
                    int min = (time2 / 60)%60;
                    int sec = time2 % 60;

                    float D = Distance( playr.Info.X, playr.Info.Y, playr2.Info.X, playr2.Info.Y) / 65536;

                    street->CurentStreetInfo(&StreetInfo, UCID2);

                    string str = StringFormat("%s^7 - %s, %0.0f ^C� ^2(^1%02d:%02d^2)", playr2.PName.c_str(), msg->_(UCID, StreetInfo.StreetName), D, min, sec);

                    if (playr2.Pogonya == 2)
                        str = StringFormat(msg->_(UCID,"2124"), playr2.PName.c_str(), msg->_(UCID, StreetInfo.StreetName));

                    insim->SendBFN(UCID,id); // ��� ���� ����� �� ����������� ������ �����
                    if(players[UCID].Info.getSpeed() < 5 && players[UCID].Barier.canAdd)
                    {
                        if(!players[UCID].Barier.added)
                            insim->SendButton(UCID2, UCID, id, 75, T, 50, 4, ISB_LIGHT + ISB_CLICK, str + msg->_(UCID,"addBarier"));
                        else
                            insim->SendButton(UCID2, UCID, id, 75, T, 50, 4, ISB_LIGHT + ISB_CLICK, str + msg->_(UCID,"delBarier"));
                    }
                    else
                        insim->SendButton(UCID2, UCID, id, 75, T, 50, 4, ISB_DARK, str);

                    T -=4;
                    ++id;
                }
            }
        }
    }

    if(this->SwitchSirena)
    {
        SetSirenLight("^4||||||||||^7|^1||||||||||");
        this->SwitchSirena = false;
    }
    else
    {
        SetSirenLight("^1||||||||||^7|^4||||||||||");
        this->SwitchSirena = true;
    }
}

bool
RCPolice::isArested(byte UCID)
{
    if(players.find(UCID) == players.end())
        return false;

    if(ArestPlayers.find(players[UCID].UName) == ArestPlayers.end())
        return false;

    if(ArestPlayers[players[UCID].UName] > time(NULL))
        return true;

    return false;
}

void
RCPolice::SaveAll()
{
    for( auto i: players)
    {
        Save(i.first);
    }
}
