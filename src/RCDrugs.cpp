#include "RCDrugs.h"
const int TimeCook = 60*10;
RCDrugs*
RCDrugs::self = nullptr;

RCDrugs*
RCDrugs::getInstance()
{
    if(!self)
        self = new RCDrugs();

    return self;
}

RCDrugs::RCDrugs()
{
    ClassName = "RCDrugs";
}

RCDrugs::~RCDrugs()
{

}

int RCDrugs::init(const char* Dir)
{
    strcpy(RootDir,Dir);

    this->insim = CInsim::getInstance();

    this->msg = RCMessage::getInstance();
    this->dl = RCDL::getInstance();
    this->bank = RCBank::getInstance();
    this->police = RCPolice::getInstance();
    this->street = RCStreet::getInstance();
    this->nrg = RCEnergy::getInstance();

    this->items = Items::Get();

    CCText("^3"+ClassName+":\t\t^2inited");

    return 1;
}

void RCDrugs::InsimCNL( struct IS_CNL* packet )
{
    players.erase( packet->UCID );
}
void RCDrugs::InsimMSO( struct IS_MSO* packet )
{
    byte UCID = packet->UCID;

    if ( UCID == 0 )
        return;

    if ( packet->UserType != MSO_PREFIX )
        return;

    xString Message = packet->Msg + packet->TextStart;
    vector<xString> args = Message.split(' ',0);

    if(police->IsCop(UCID) && args.size() > 1 && args[0] == "!s")
    {
        return;

        players[UCID].SearchUname = args[1];
        players[UCID].CopSearch = true;
    }

    if(Message == "!buy")
    {
        if(police->IsCop(UCID))
            return;

        if(players[UCID].dealerName.size() == 0)
            return;

        if(!config["dealer"].isMember(players[UCID].dealerName))
            return;

        auto dealer = config["dealer"][players[UCID].dealerName];

        if(dealer["workNow"].asInt() == 0)
        {
            // insim->SendMTC(UCID, msg->_(UCID,"DealerOffline"));
           return;
        }

        if(bank->GetCash(UCID) < dealer["price"].asInt())
        {
            insim->SendMTC(UCID, msg->_(UCID,"NoManyPay"));
            return;
        }

        else if(items->getItemsByOwner(xString(players[UCID].UName).toLower(), dealer["product"].asString()).size() > 0)
        {
            insim->SendMTC(UCID, msg->_(UCID,"DealerYouHaveDrug"));
            return;
        }

        int ID = items->Add(dealer["product"].asString(), "Drug");

        if(ID > 0)
        {
            Item* item = items->GetItem(ID);
            item->setOwner(players[UCID].UName)->setCarrier(players[UCID].UName)->setUseCount(1)->Save();
            insim->SendMTC(UCID, msg->_(UCID,"Buy") + dealer["product"].asString());
            bank->RemCash(UCID, dealer["price"].asInt());
            bank->AddToBank(dealer["price"].asInt());
        }
       return;


    }

    if(Message == "!drug")
    {

        if(police->IsCop(UCID))
            {
                insim->SendMTC(UCID, msg->_(UCID,"DrugHelpSearch"));
                return;
            }
        // send help usage commands (cook, use, hide, take, put, search(for cop))
            insim->SendMTC(UCID, msg->_(UCID,"DragHelp"));
            insim->SendMTC(UCID, msg->_(UCID,"DragHelpUse"));
            insim->SendMTC(UCID, msg->_(UCID,"DrugHelpCook"));
            insim->SendMTC(UCID, msg->_(UCID,"DrugHelpInventory"));

    }
    else if(args.size() > 1 && args[0] == "!drug")
    {
        if(args[1] == "cook")
        {
             if(police->IsCop(UCID))
                return;
          if(!players[UCID].InTrack)
            {
              return;
            }

           if(police->IsCop(UCID))
                return;

            if(players[UCID].PlayerCooking)
            {
                insim->SendMTC(UCID, msg->_(UCID,"isCook"));
                return;
            }

          if(items->getItemsByOwner(xString(players[UCID].UName).toLower(),"Phenylacetone").size() > 0 && items->getItemsByOwner(xString(players[UCID].UName).toLower(),"FormicAcid").size() > 0)
          {

                players[UCID].PlayerCooking  = true;
                players[UCID].CookTime = time(nullptr) + TimeCook;
                insim->SendMTC(UCID, msg->_(UCID,"StartCook"));
                insim->SendMTC(UCID, msg->_(UCID,"StartCook1"));
          }
            else
                insim->SendMTC(UCID, msg->_(UCID,"YouHaveNotElements"));
                return;
        }

        else if(args[1] == "use")
        {
             if(police->IsCop(UCID))
                return;
            vector<int> itemIDs = items->getItemsByOwner(xString(players[UCID].UName).toLower(),"dose");
            if(itemIDs.size() <= 0)
            {
                insim->SendMTC(UCID, msg->_(UCID,"DragYouHaveNotDrug"));
                return;
            }
            if(nrg->Islocked(UCID))
            {
                int itemID = itemIDs.front();
                Item *it = items->GetItem(itemID);
                if(it->Use())
                {
                    insim->SendMTC(UCID, msg->_(UCID,"DragYouOverdose"));
                    nrg->RemoveEnergy(UCID, 185);
                    nrg->LockTo(UCID, time(nullptr));
                    return;
                }
            }
            else
            {
                int itemID = itemIDs.front();
                Item *it = items->GetItem(itemID);
                if(it->Use())
                {
                    nrg->AddEnergy(UCID, MAX_ENERGY);
                    nrg->LockTo(UCID, time(nullptr) + 60*15);
                }
                insim->SendMTC(UCID, msg->_(UCID,"DragYouUseDrag"));
                return;
            }
        }
        else if(args[1] == "inv")
        {
            if(police->IsCop(UCID))
                return;


            int DoseSize = items->getItemsByOwner(xString(players[UCID].UName).toLower(),"dose").size();
            int PhenylacetoneSize = items->getItemsByOwner(xString(players[UCID].UName).toLower(),"Phenylacetone").size();
            int FormicAcidSize = items->getItemsByOwner(xString(players[UCID].UName).toLower(),"FormicAcid").size();

            if(DoseSize || PhenylacetoneSize || FormicAcidSize)
                insim->SendMTC(UCID, StringFormat(msg->_(UCID,"DragInventory"),""));

            if(DoseSize > 0)
            {
                string str = StringFormat("^2| ^7%s ^7[^2%d^7]", msg->_(UCID,"dose"),DoseSize);
                insim->SendMTC(UCID, str);
            }

            if(PhenylacetoneSize > 0)
            {
                string str = StringFormat("^2| ^7%s ^7[^2%d^7]",msg->_(UCID,"element1"),PhenylacetoneSize);
                insim->SendMTC(UCID, str);
            }

            if(FormicAcidSize > 0)
            {
                string str = StringFormat("^2| ^7%s ^7[^2%d^7]",msg->_(UCID,"element2"),FormicAcidSize);
                insim->SendMTC(UCID, str);
            }
        }
        else if(args[1] == "help")
        {
             if(police->IsCop(UCID))
            {
                insim->SendMTC(UCID, msg->_(UCID,"DrugHelpSearch"));
                return;
            }
                insim->SendMTC(UCID, msg->_(UCID,"DragHelp"));
                insim->SendMTC(UCID, msg->_(UCID,"DragHelpUse"));
                insim->SendMTC(UCID, msg->_(UCID,"DrugHelpCook"));
                insim->SendMTC(UCID, msg->_(UCID,"DrugHelpInventory"));
        }
    }

}
void RCDrugs::InsimNCN( struct IS_NCN* packet )
{
    if ( packet->UCID == 0 )
    {
        return;
    }
    players[packet->UCID].UName = packet->UName;
    players[packet->UCID].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;

    if(!packet->Admin)
    {
//        players[packet->UCID].Admin = this->isAdmin(packet->UName, this->ClassName);
    }

}

void RCDrugs::ReadConfig(const char *Track)
{
    config.clear();
    char file[255];
    sprintf(file, "%s/RCDrugs/%s.json", RootDir, Track);

    ifstream readf (file, ios::in);

    if(!readf.is_open())
    {
        CCText("  ^7RCDrugs    ^1ERROR: ^8file " + (string)file + " not found");
        return ;
    }

    bool readed = configReader.parse( readf, config, false );

    readf.close();

    if ( !readed )
    {
        // report to the user the failure and their locations in the document.
        cout  << "Failed to parse configuration\n"
              << configReader.getFormattedErrorMessages();
        return;
    }

    for(string dealerName: config["dealer"].getMemberNames())
    {
        Marshal(config["dealer"][dealerName], false);
    }
    sprintf(file, "%s/RCDrugs/lang", RootDir);
    msg->ReadLangDir(file);

    CCText("  ^7RCDrugs\t^2OK");
}
void RCDrugs::InsimMCI( struct IS_MCI* packet )
{
    for (int i = 0; i < packet->NumC; i++) // ?????? ?? ????? ??????? packet->Info[i]
    {
        byte UCID = PLIDtoUCID[ packet->Info[i].PLID ];
        int X = packet->Info[i].X/65536;
        int Y = packet->Info[i].Y/65536;
        int Speed = ((int)packet->Info[i].Speed * 360) / (32768);
        players[UCID].Info = packet->Info[i];

        if(players[UCID].PlayerCooking)
        {
            if(Speed > 5)
            {
                players[UCID].CookFail = true;
                insim->SendMTC(UCID, msg->_(UCID,"CookFail"));
            }
        }

        if(Speed > 0)
        {
              players[UCID].CanSearch = false;
        }
        else
            players[UCID].CanSearch = true;

        bool finded = false;
        for(string dealerName: config["dealer"].getMemberNames())
        {
            auto dealer = config["dealer"][dealerName];

            if(dealer["workNow"].asInt() == 0)
                continue;

            int DealerX = dealer["x"].asInt();
            int DealerY = dealer["y"].asInt();
            int dist = Distance(X , Y , DealerX , DealerY );

            if(dist <= dealer["distance"].asInt())
            {
                finded = true;
                if (players[UCID].dealerName.empty())
                {
                    players[UCID].dealerName = dealer["name"].asString();

                    insim->SendMTC(UCID, StringFormat( msg->_(UCID,"Drug1"), dealer["name"].asCString()));
                    insim->SendMTC(UCID, StringFormat( msg->_(UCID,"Drug2"), msg->_(UCID,dealer["product"].asCString())));
                    insim->SendMTC(UCID, StringFormat( msg->_(UCID,"Drug3"), dealer["price"].asInt()));

                }

                break;
            }
        }

        if (!finded && !players[UCID].dealerName.empty())
            players[UCID].dealerName.clear();
    }


}
void RCDrugs::InsimCPR( struct IS_CPR* packet )
{
    players[packet->UCID].PName = packet->PName;
}
void RCDrugs::InsimNPL( struct IS_NPL* packet )
{
     PLIDtoUCID[packet->PLID] = packet->UCID;
     players[packet->UCID].InTrack = true;
}
void RCDrugs::InsimPLL( struct IS_PLL* packet )     // ????? ???? ? ???????
{
    byte UCID = PLIDtoUCID[packet->PLID];
    players[UCID].InTrack = false;
    if(players[UCID].PlayerCooking)
    {
       players[UCID].CookFail = true;
       insim->SendMTC(UCID, msg->_(UCID,"CookFail"));
    }
    PLIDtoUCID.erase( packet->PLID );
}

void RCDrugs::InsimPLP( struct IS_PLP* packet )     // ????? ???? ? ?????
{
    byte UCID = PLIDtoUCID[packet->PLID];
    players[UCID].InTrack = false;
    if(players[UCID].PlayerCooking)
    {
       players[UCID].CookFail = true;
       insim->SendMTC(UCID, msg->_(UCID,"CookFail"));
    }
}

void RCDrugs::InsimCON( struct IS_CON* packet )
{
    byte UCIDA = PLIDtoUCID[ packet->A.PLID ];
    byte UCIDB = PLIDtoUCID[ packet->B.PLID ];

    if(players[UCIDA].PlayerCooking)
    {
        players[UCIDA].CookFail = true;
        insim->SendMTC(UCIDA, msg->_(UCIDA,"CookFail"));
        return;
    }

    if(players[UCIDB].PlayerCooking)
    {
        players[UCIDB].CookFail = true;
        insim->SendMTC(UCIDB, msg->_(UCIDB,"CookFail"));
        return;
    }

}

void RCDrugs::InsimOBH( struct IS_OBH* packet )
{
    byte UCID = PLIDtoUCID[ packet->PLID ];
    if(players[UCID].PlayerCooking)
    {
        players[UCID].CookFail = true;
        insim->SendMTC(UCID, msg->_(UCID,"CookFail"));
        return;
    }

}

void RCDrugs::Event()
{
    struct tm *lt = tools::GetLocalTime();

    for(string dealerName: config["dealer"].getMemberNames())
    {
        auto& dealer = config["dealer"][dealerName];
        bool canWork = false;

        for(auto hour: dealer["workhours"])
        {
            if(hour.asInt() == lt->tm_hour)
            {
                canWork = true;
                break;
            }
        }

        if(canWork)
        {
            if(dealer["workNow"].asInt() == 0)
                Marshal(dealer, true);

            dealer["workNow"] = 1;
        }
        else
        {
            if(dealer["workNow"].asInt() != 0)
                Marshal(dealer, false);

            dealer["workNow"] = 0;
        }
    }

    for ( auto play: players )
    {
        if(players[play.first].CopSearch)
        {
            for ( auto pl: players )
            {
                string str;
                if(players[pl.first].UName == players[play.first].SearchUname && players[pl.first].CanSearch)
                {
                    int Dist = Distance(players[play.first].Info.X/65536, players[play.first].Info.Y/65536, players[pl.first].Info.X/65536, players[pl.first].Info.Y/65536);
                    if(Dist < 10)
                    {

                        str = StringFormat(msg->_(pl.first,"PlayerYouAreSearch"), players[play.first].PName.c_str());
                        insim->SendMTC(pl.first,str);

                        str = StringFormat(msg->_(play.first,"CheckOk"), players[pl.first].PName.c_str());
                        insim->SendMTC(play.first,str);

                        vector<int> Doses = items->getItemsByOwner(xString(players[pl.first].UName).toLower(),"dose");
                        int PhenylacetoneSize = items->getItemsByOwner(xString(players[pl.first].UName).toLower(),"Phenylacetone").size();
                        int FormicAcidSize = items->getItemsByOwner(xString(players[pl.first].UName).toLower(),"FormicAcid").size();

                        if(Doses.size() || PhenylacetoneSize ||FormicAcidSize)
                            insim->SendMTC(play.first, msg->_(play.first,"DragInventory"));

                        if(Doses.size() > 0)
                        {
                            str = StringFormat("^2| ^7%s ^7[^2%d^7]", msg->_(play.first,"dose"),Doses.size());
                            insim->SendMTC(play.first, str);
                        }

                        if(PhenylacetoneSize > 0)
                        {
                            str = StringFormat("^2| ^7%s ^7[^2%d^7]",msg->_(play.first,"element1"),PhenylacetoneSize);
                            insim->SendMTC(play.first, str);
                        }

                        if(FormicAcidSize > 0)
                        {
                            str = StringFormat("^2| ^7%s ^7[^2%d^7]",msg->_(play.first,"element2"),FormicAcidSize);
                            insim->SendMTC(play.first, str);
                        }

                        if(!police->IsCop(pl.first) && nrg->Islocked(pl.first))
                        {
                            insim->SendMTC(play.first, msg->_(play.first,"PlayerUnderDrugs"));
                        }

                        if(players[pl.first].PlayerCooking)
                        {
                            insim->SendMTC(play.first, msg->_(play.first,"PlayerSearchCook"));
                            players[pl.first].CookFail = true;
                            insim->SendMTC(pl.first, msg->_(pl.first,"CookFail"));
                        }

                        if(Doses.size() > 0)
                        {
                            str = StringFormat(msg->_(pl.first,"CopConfiscation"), Doses.size());
                            insim->SendMTC(pl.first, str);
                            str = StringFormat(msg->_(pl.first,"YouConfiscation"), Doses.size());
                            insim->SendMTC(play.first, str);

                            for(auto i = 0; i < Doses.size(); i++)
                            {
                                 items->Delete(Doses[i]);
                            }
                        }

                        players[play.first].CopSearch = false;
                        players[pl.first].CanSearch = false;
                    }
                    else
                    {
                        insim->SendMTC(play.first, msg->_(play.first,"Dist1"));
                        players[play.first].CopSearch = false;
                        players[pl.first].CanSearch = false;
                    }

                    break;
                }
            }
        }

        if(players[play.first].PlayerCooking )
        {
            int T = players[play.first].CookTime - time(NULL);

            string str = StringFormat(msg->_(play.first,"DragCookTime"),(T/60)%60, T%60);
            insim->SendButton(255, play.first, 207, 1, 125, 20, 4, 32+2, str);

            if(players[play.first].CookTime < time(nullptr))
            {

                int itemID1 = items->getItemsByOwner(xString(players[play.first].UName).toLower(),"Phenylacetone").front();
                int itemID2 = items->getItemsByOwner(xString(players[play.first].UName).toLower(),"FormicAcid").front();

                Item *Phenyacetone = items->GetItem(itemID1);
                Item *FormicAcid = items->GetItem(itemID2);

                if(Phenyacetone->Use() && FormicAcid->Use())
                {
                    insim->SendBFN(play.first, 207);
                    players[play.first].PlayerCooking = false;
                    players[play.first].CookPassed = false;
                    players[play.first].CookFail = false;
                    players[play.first].CookPassed = true;
                    insim->SendMTC(play.first, msg->_(play.first,"CookPassed"));
                    int ID = items->Add("dose","Drug");
                    if(ID > 0)
                    {
                        Item* item = items->GetItem(ID);
                        item->setOwner(players[play.first].UName)->setCarrier(players[play.first].UName)->setUseCount(1)->Save();
                    }

                }
            }
        }


        if(players[play.first].CookFail)
        {
            insim->SendBFN(play.first, 207);

            int itemID1 = items->getItemsByOwner(xString(players[play.first].UName).toLower(),"Phenylacetone").front();
            int itemID2 = items->getItemsByOwner(xString(players[play.first].UName).toLower(),"FormicAcid").front();

            Item *Phenyacetone = items->GetItem(itemID1);
            Item *FormicAcid = items->GetItem(itemID2);

            if(Phenyacetone->Use() && FormicAcid->Use())
            {
                insim->SendMTC(play.first, msg->_(play.first,"CookFail1"));
                players[play.first].PlayerCooking = false;
                players[play.first].CookPassed = false;
                players[play.first].CookFail = false;
            }
        }
    }
}

void RCDrugs::Marshal(Json::Value dealer, bool on)
{
    if(on)
    {
        ObjectInfo *obj = new ObjectInfo;
        obj->Index = 254;
        obj->X = dealer["x"].asInt() * 65536 / 4096;
        obj->Y = dealer["y"].asInt() * 65536 / 4096;
        obj->Zbyte = dealer["z"].asInt() * 65536 / 4096;
        obj->Heading = dealer["heading"].asInt() * 256 / 360;
        obj->Flags = 5;
        AddObject(obj);
        delete obj;
    }
    else
    {
        ObjectInfo *obj = new ObjectInfo;
        obj->Index = 254;
        obj->X = dealer["x"].asInt() * 65536 / 4096;
        obj->Y = dealer["y"].asInt() * 65536 / 4096;
        obj->Zbyte = dealer["z"].asInt() * 65536 / 4096;
        obj->Heading = dealer["heading"].asInt() * 256 / 360;
        obj->Flags = 5;
        DelObject(obj);
        delete obj;
    }
}
