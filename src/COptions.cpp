#include "COptions.h"

COptions* COptions::self = nullptr;
string COptions::rootPath = "";
Json::Value COptions::config = Json::objectValue;

COptions::COptions()
{
    readed = false;
    load();
}

COptions::~COptions()
{
    save();
}

void
COptions::setRoot(string path)
{
    COptions::rootPath = path;
}

COptions*
COptions::getInstance()
{
    if(!self)
    {
        //cout << "COptions: create new instance" << endl;
        self = new COptions();
    }
    return self;
}

void
COptions::load()
{
    if(rootPath.empty())
    {
        throw new logic_error("COptions Root Path Not Set");
    }

    config.clear();
    char file[255];
    sprintf(file, "%s/%s", rootPath.c_str(), filename.c_str());

    ifstream readf (file, ios::in);

    if(!readf.is_open())
    {
        cout << "COptions: ^8file " << file << " not found" << endl;
        return;
    }

    readed = configReader.parse( readf, config, false );

    readf.close();

    if ( !readed )
    {
        // report to the user the failure and their locations in the document.
        cout << "Failed to parse configuration\n" << configReader.getFormattedErrorMessages() << endl;
    }
}

void
COptions::save()
{
    char file[255];
    sprintf(file, "%s/%s", rootPath.c_str(), filename.c_str());
	ofstream f;
	f.open(file, ios::out);
	f << configWriter.write( config );
	f.close();
}

bool
COptions::isMember(string module, string paramName)
{
    if(!config.isObject())
        config = Json::objectValue;

    if(!config.isMember(module))
        return false;

    if(!config[module].isMember(paramName))
        return false;

    return true;
}

vector<string>
COptions::getModules()
{
    return config.getMemberNames();
}

vector<string>
COptions::getParams(string module)
{
    vector<string> result;

    if(config.isMember(module))
        result = config[module].getMemberNames();

    return result;
}

string
COptions::getType(string module, string paramName)
{
    if(!config.isObject())
        config = Json::objectValue;

    if(!config.isMember(module))
        throw new logic_error("COptions dont have " + module + " module");

    if(!config[module].isMember(paramName))
        throw new logic_error("COptions dont have " + paramName + " in " + module + " module");

    if(config[module][paramName].isString())
        return "string";

    if(config[module][paramName].isInt())
        return "int";

    if(config[module][paramName].isDouble())
        return "double";

    return "";
}

string
COptions::getString(string module, string paramName)
{
    getInstance(); // load protection

    if(!config.isObject())
        config = Json::objectValue;

    if(!config.isMember(module))
        throw new logic_error("COptions dont have " + module + " module");

    if(!config[module].isMember(paramName))
        throw new logic_error("COptions dont have " + paramName + " in " + module + " module");

    return config[module][paramName].asString();
}

int
COptions::getInt(string module, string paramName)
{
    getInstance(); // load protection

    if(!config.isObject())
        config = Json::objectValue;

    if(!config.isMember(module))
        throw new logic_error("COptions dont have " + module + " module");

    if(!config[module].isMember(paramName))
        throw new logic_error("COptions dont have " + paramName + " in " + module + " module");

    return config[module][paramName].asInt();
}

double
COptions::getDouble(string module, string paramName)
{
    getInstance(); // load protection

    if(!config.isObject())
        config = Json::objectValue;

    if(!config.isMember(module))
        throw new logic_error("COptions dont have " + module + " module");

    if(!config[module].isMember(paramName))
        throw new logic_error("COptions dont have " + paramName + " in " + module + " module");

    return config[module][paramName].asDouble();
}

bool
COptions::Set(string module, string paramName, string value)
{
    getInstance(); // load protection
    if(!config.isObject())
        config = Json::objectValue;

    if(!config.isMember(module))
        config[module] = Json::objectValue;

    config[module][paramName] = value;

    CLog::log("COptions", "info", "set "+module+"["+paramName+"] to " + value);

    getInstance()->save();
    return true;
}

bool
COptions::Set(string module, string paramName, int value)
{
    getInstance(); // load protection

    if(!config.isObject())
        config = Json::objectValue;

    if(!config.isMember(module))
        config[module] = Json::objectValue;

    config[module][paramName] = value;

    CLog::log("COptions", "info", "set "+module+"["+paramName+"] to " + ToString(value));

    getInstance()->save();
    return true;
}

bool
COptions::Set(string module, string paramName, double value)
{
    getInstance(); // load protection

    if(!config.isObject())
        config = Json::objectValue;

    if(!config.isMember(module))
        config[module] = Json::objectValue;

    config[module][paramName] = value;

    CLog::log("COptions", "info", "set "+module+"["+paramName+"] to " + ToString(value));

    getInstance()->save();
    return true;
}

bool
COptions::Delete(string module, string paramName)
{
    getInstance(); // load protection
    config[module].removeMember(paramName);

    if(config[module].size() == 0)
        config.removeMember(module);

    getInstance()->save();
    return true;
}
