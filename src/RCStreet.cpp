using namespace std;
#include "RCStreet.h"

RCStreet*
RCStreet::self = nullptr;

RCStreet*
RCStreet::getInstance()
{
    if(!self)
        self = new RCStreet();

    return self;
}

RCStreet::RCStreet()
{
    ClassName = "RCStreet";
    memset(&Street, 0, sizeof( struct streets ) * MAX_STREETS );
}

RCStreet::~RCStreet()
{
}

int RCStreet::init(const char* Dir)
{
    strcpy(RootDir,Dir);

    IfInited = false;

    this->db = DBMySQL::Get();
    if (!this->db)
    {
        printf("RCStreet: Can't sctruct MySQL Connector\n");
        return -1;
    } // �������� ���� �� ���������

    insim = CInsim::getInstance(); // ����������� ��������� ������� ������
    if (!insim) // ��������� �� �������������
    {
        printf ("RCStreet: Can't struct CInsim class");
        return -1;
    }

    msg = RCMessage::getInstance();
    if (!msg)
    {
        printf ("Can't struct RCMessage class");
        return -1;
    }

    IfInited = true;

    CCText("^3"+ClassName+":\t^2inited");
    return 0;
}

const char* RCStreet::GetStreetName(byte UCID, int StreetID)
{
    if (StreetID >= 0)
        return msg->_(UCID, Street[StreetID].StreetName);
    else
        return "[NULL]";
}

int RCStreet::GetStreetId(int X, int Y)
{
    for (int i = 0; i < StreetNums; i++)
        if (CheckPosition(Street[i].PointCount, Street[i].StreetX, Street[i].StreetY, X, Y))
            return i;
    return DEFAULT_STREET_NUM;
}

void RCStreet::ReadConfig(const char *Track)
{
    char file[255];

    sprintf(file, "%s/RCStreet/%s/lang", RootDir, Track);
	msg->ReadLangDir(file);

    sprintf(file, "%s/RCStreet/%s/streets.json", RootDir, Track);

    ifstream readf (file, ios::in);

    if (readf.is_open() == false)
    {
        CCText("  ^7RCStreet   ^1ERROR: ^8file " + (string)file + " not found");
        return;
    }

    bool readed = configReader.parse( readf, config, false );

    if ( !readed )
	{
		readf.close();
		// report to the user the failure and their locations in the document.
		cout  << "Failed to parse configuration\n"
				   << configReader.getFormattedErrorMessages();
		return;
	}
	readf.close();

	if(!config.isArray())
    {
        cout  << "config must be array\n";
        return;
    }

    int i = 0;
    for (Json::Value jsStreet: config)
    {
        //������� �������
        memset(&Street[i], 0, sizeof(streets));

        Street[i].StreetID = i;

        // ������ �������� �����
        strcpy(Street[i].StreetName, jsStreet["name"].asCString());

        Street[i].SpeedLimit = jsStreet["speedLimit"].asInt();

        Street[i].PointCount = jsStreet["X"].size();

        if ( Street[i].StreetX != NULL )
            delete[] Street[i].StreetX;

        if ( Street[i].StreetY != NULL )
            delete[] Street[i].StreetY;

        // ������ ���������
        Street[i].StreetX = new int[Street[i].PointCount];
        Street[i].StreetY = new int[Street[i].PointCount];

        for (int k=0; k<Street[i].PointCount; k++)
        {
            Street[i].StreetX[k] = jsStreet["X"][k].asInt();
            Street[i].StreetY[k] = jsStreet["Y"][k].asInt();
        }

        i++;

    }
    StreetNums = i;

    readf.close();

    CCText("  ^7RCStreet\t^2OK");
}

void RCStreet::InsimCNL( struct IS_CNL* packet )
{
    players.erase(packet->UCID);
}

void RCStreet::InsimCPR( struct IS_CPR* packet )
{
    players[ packet->UCID ].PName = packet->PName;
}

void RCStreet::InsimMCI ( struct IS_MCI* pack_mci )
{
    for (int i = 0; i < pack_mci->NumC; i++)
    {
        byte UCID = PLIDtoUCID[ pack_mci->Info[i].PLID ];

        int X = pack_mci->Info[i].X/65536;
        int Y = pack_mci->Info[i].Y/65536;

        int CurrentID = GetStreetId(X, Y);

        if (players[UCID].StreetNum != CurrentID && CurrentID < DEFAULT_STREET_NUM)
            players[UCID].StreetNum = CurrentID;

        BtnStreet( UCID );
    }
}

void RCStreet::InsimMSO( struct IS_MSO* packet )
{
    if (packet->UCID == 0)
        return;
}


void RCStreet::InsimNCN( struct IS_NCN* packet )
{
    if (packet->UCID == 0)
        return;

    players[ packet->UCID ].UName = packet->UName;
    players[ packet->UCID ].PName = packet->PName;
    players[packet->UCID].Admin = packet->Admin;
}

void RCStreet::InsimNPL( struct IS_NPL* packet )
{
    PLIDtoUCID[ packet->PLID ] = packet->UCID;
    players[ packet->UCID ].StreetNum = DEFAULT_STREET_NUM;
}

void RCStreet::InsimPLP( struct IS_PLP* packet)
{
    PLIDtoUCID.erase( packet->PLID );
}

void RCStreet::InsimPLL( struct IS_PLL* packet )
{
    PLIDtoUCID.erase( packet->PLID );
}

void RCStreet::BtnStreet (byte UCID)
{
    if (players[ UCID ].StreetNum > StreetCount())
        return;

    insim->SendButton(255, UCID, 171, 170, 8, 29, 6, 32 + 3, GetStreetName(UCID, players[UCID].StreetNum));
    insim->SendButton(255, UCID, 172, 156, 2, 21, 19, 0, "^1^S��");
    insim->SendButton(255, UCID, 173, 156, 7, 21, 8, 3, ToString(Street[ players[ UCID ].StreetNum ].SpeedLimit));
}

int RCStreet::CurentStreetNum(byte UCID)
{
    return players[ UCID ].StreetNum;
}

int RCStreet::CurentStreetInfo(void *StreetInfo, byte UCID)
{
    if ( players[ UCID ].StreetNum == DEFAULT_STREET_NUM )
        return -1;

    if (memcpy(StreetInfo, &Street[ players[ UCID ].StreetNum ], sizeof(streets)))
        return 1;

    return 1;
}

int RCStreet::CurentStreetInfoByNum(void *StreetInfo, int StrNum)
{
    if (memcpy(StreetInfo, &Street[StrNum], sizeof(streets)))
        return 1;

    return -1;
}

int RCStreet::StreetCount()
{
    return StreetNums;
}
