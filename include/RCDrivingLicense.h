#ifndef _RC_LEVEL_H
#define _RC_LEVEL_H

#include "RCBaseClass.h"
#include "tools.h"
#include "RCBaseClass.h"
#include "RCMessage.h"




struct DLPlayer : GlobalPlayer
{
    u_int   LVL;
    float   Skill;
    bool 	Lock;

    float   SkillCoef;
    time_t  SkillBonusTimeout;
};

class RCDL:public RCBaseClass
{
private:
    RCDL();
    ~RCDL();
    static RCDL* self;

    RCMessage *msg;

    map<byte, DLPlayer>players;     // Array of players

    // функции-повторители основных фунцкий ядра
    void InsimCNL( struct IS_CNL* packet );
    void InsimCON( struct IS_CON* packet );
    void InsimCPR( struct IS_CPR* packet );
    void InsimMCI( struct IS_MCI* packet );
    void InsimMSO( struct IS_MSO* packet );
    void InsimNCN( struct IS_NCN* packet );
    void InsimNPL( struct IS_NPL* packet );
    void InsimPLL( struct IS_PLL* packet );
    void InsimPLP( struct IS_PLP* packet );
    void InsimOBH( struct IS_OBH* packet );

public:
    static RCDL* getInstance();
    //
    void    Save(byte UCID);
    void SaveAll();

    int     GetLVL(byte UCID);
    int     GetSkill(byte UCID);
    bool    AddSkill(byte UCID, float coef = 1);
    bool    RemSkill(byte UCID, float coef = 1);
    bool    Lock(byte UCID);
    bool    Unlock(byte UCID);
    bool    Islocked(byte UCID);

    byte    inited;

    void    Event();

    // Основные функции класса
    int init(const char* Dir);
};

#endif
