#ifndef _RC_MESSAGE_H
#define _RC_MESSAGE_H

#include "RCBaseClass.h"
#include "tools.h"

#include <dirent.h>

struct MPlayer: public GlobalPlayer
{
    string Lang;
    queue<string> notifications;
};

class RCMessage:public RCBaseClass
{
private:
    RCMessage();
    ~RCMessage();
    static RCMessage* self;
    string color = "1";

    string DEFAULT_LANGUAGE;

    map <string, map <string, string>> MsgArray;

    map <byte, MPlayer> players;

    void InsimNCN( struct IS_NCN* packet );
    void InsimNCI( struct IS_NCI* packet );
    void InsimCNL( struct IS_CNL* packet );
    void InsimMSO( struct IS_MSO* packet );
    void InsimBTC( struct IS_BTC* packet );

    void ReadSwearCharList();
    map <char, char> CharList;
    void ReadSwearList();
    vector <string> SwearList;           // ������� �����

public:
    static RCMessage* getInstance();

    int init(const char* Dir);
    void ReadConfig(const char *Track);

    string GetLang(byte UCID);

    void Save(byte UCID);
    void SaveAll();

    const char* _( byte UCID, string CODE );

    void ReadLangDir(xString path );
    void ReadLangFile(xString file);
    string GetLangList();
    list<string> GetLangArray();

    int CheckSwear(string Text);        // �������� �� ����, ���������� �� ���-��

    void AddNotify(byte UCID, string Notify);
    void ShowNotify(byte UCID);

    void Event();

};

#endif
