#ifndef RCITEMS_H
#define RCITEMS_H

#include <RCBaseClass.h>
#include <RCMessage.h>
#include "Items.h"
struct ItemPlayer: GlobalPlayer
{

};

class RCItems : public RCBaseClass
{
    public:
        static RCItems* getInstance();
        int init(const char* Dir);
        void Event();

    private:
        RCItems();
        ~RCItems();
        static RCItems* self;

        map<byte, ItemPlayer> players;

        void InsimNCN( struct IS_NCN* packet );
        void InsimCNL( struct IS_CNL* packet );
        void InsimMCI( struct IS_MCI* packet );
        void InsimNPL( struct IS_NPL* packet );
        void InsimPLL( struct IS_PLL* packet );
        void InsimPLP( struct IS_PLP* packet );

        void InsimMSO( struct IS_MSO* packet );
};

#endif // RCITEMS_H
