#ifndef _RC_CORE_H
#define _RC_CORE_H

#include <unordered_map>

#include <RCBaseClass.h>
#include <RCMessage.h>
#include <RCBank.h>
#include <RCPolice.h>
#include <RCStreet.h>
#include <RCEnergy.h>
#include <RCDrivingLicense.h>

#include <Items.h>
#include <version.h>

#define BONUS_DISTANCE 5000
#define BONUS_TIMEOUT 600 // 10 minutes


struct user_car
{
    int             tuning;
    float           dist;
    bool            rent;
    float           rentDistance;
};

struct track_info
{
    int     PitCount;
    int     *XPit = NULL;
    int     *YPit = NULL;
    int     ShopCount;
    int     *XShop = NULL;
    int     *YShop = NULL;
    int     CafeCount;
    int     *XCafe = NULL;
    int     *YCafe = NULL;
};


struct player: public GlobalPlayer
{
    map<string, user_car> cars;


    float   Distance;               // ��� ������� ������

    bool    ShowKM = false;
    byte    Zone;

    byte    debug;                  // ������ � ������������ ������ !debug

    int     FloodCount;             // ���-�� ��������� ������ � ������������� ���������
    int     FloodTime;              // ����� ���������� ��������� ��� �������� ���������

    time_t  LastSave;
    time_t  LastBFN;                // TODO: ��������� �� ����� ���/����

    unsigned PLC;                   // ������ ������ �� ��������� ����
    byte    PenReason;              // From IS_PEN

    time_t  PLL_time;               // ������� ��� ���������� ������
};

class RCCore: public RCBaseClass
{
private:
    RCCore();
    ~RCCore();
    static RCCore* self;

    RCMessage*  msg;
    RCBank*     bank;
    RCPolice*   police;
    RCStreet*   street;
    RCEnergy*   nrg;
    RCDL*       dl;

    map <byte, player> players;
    Json::Value cars;

    struct  track_info TrackInf;             // Where PitBox and Shop

    /** IS_RST **/
    string  Track;                          // Current track (for streets)

    void InsimBFN( struct IS_BFN* packet );
    void InsimBTC( struct IS_BTC* packet );
    void InsimBTT( struct IS_BTT* packet );
    void InsimCNL( struct IS_CNL* packet );
    void InsimTOC( struct IS_TOC* packet );
    void InsimCPR( struct IS_CPR* packet );
    void InsimMCI( struct IS_MCI* packet );
    void InsimMSO( struct IS_MSO* packet );
    void InsimNCN( struct IS_NCN* packet );
    void InsimNPL( struct IS_NPL* packet );
    void InsimPEN( struct IS_PEN* packet );
    void InsimPLL( struct IS_PLL* packet );
    void InsimPLP( struct IS_PLP* packet );
    void InsimVTN( struct IS_VTN* packet );

    void InfoPage (byte UCID, byte b_type);
    void help_cmds (byte UCID, byte h_type);

    void ShowUsersList(byte UCID);
    void read_car();
    void read_track();
    void save_car (byte UCID);
    void read_user_cars(byte UCID);
    void save_user_cars(byte UCID);

    Json::Value 		bonuses;
    Json::Reader 		bonusesReader;
    Json::StyledWriter 	bonusesWriter;


public:
    static RCCore* getInstance();

    static const word isf_flag = ISF_MCI + ISF_CON + ISF_OBH + ISF_HLV + ISF_AXM_EDIT + ISF_AXM_LOAD + ISF_REQ_JOIN;
    static string IS_PRODUCT_NAME;

    int init(const char* Dir);
    void ReadConfig(const char* Track);
    void Event();

    void ReadBonuses();
    void SaveBonuses();

    void Save(byte UCID);
    void SaveAll();
    bool isAdmin(byte UCID);
    bool CanSave(byte UCID);
    bool inShop(byte UCID);
    Json::Value getCars() {return this->cars;};
};


#endif
