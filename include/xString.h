#ifndef __X_STRING
#define __X_STRING

using namespace std;

#include <memory>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdio>
#include <cstdarg>
#include <cstring>

//#include <boost/format.hpp>

class xString : public string
{
    vector<xString> flds;
public:
    xString(char *s) : string(s) { };
    xString(const char *s) : string(s) { };
    xString(string s) : string(s) { };
    xString() : string() { };
    vector<xString>& split( const char delim, int rep=1);
    static string join(vector<string> v, string glue);
    string trim();

    int asInt();
    long int asLong();
    double asDouble();
    float asFloat();

    string toLower();
    string toUpper();
};

string ToString (int i);
string ToString (unsigned int i);
string ToString (long unsigned int i);
string ToString (unsigned char b);
string ToString (bool b);
string ToString (float f);
string ToString (double d);
string ToString (const char *c);

string StringFormat(const string fmt_str, ...);

#endif
