#ifndef _RC_TAXI_H // ��������, ����� ���� ����������� ���� ���
#define _RC_TAXI_H

#include "RCBaseClass.h"

#include "RCMessage.h"  // Messages
#include "RCBank.h"     // Bank
#include "RCDrivingLicense.h"
#include "RCStreet.h"
#include "RCPolice.h"
#include "RCLight.h"

#include "tools.h"      // Check_Pos  etc.

#define MAX_POINTS 2048
#define PASSANGER_INTERVAL 1200
#define MAX_PASS_STRESS 1000

#define TAXI_PREFIX "^3[^7TAXI^3]"

struct Taxi_info
{
    int     ShopCount;
    int     XShop[10];
    int     YShop[10];
};

struct Taxi_points
{
    byte    Id;
    int     StreetId;
    int     X;
    int     Y;
};

struct Taxi_clients
{
    int     X;
    int     Y;
    int     Z;
    int     Dir;
    int     StreetId;
};


// ������ ��������� ������

struct TaxiPlayer: public GlobalPlayer
{
    byte    Zone;

    /** Work **/
    bool 	CanWork;
    bool 	HandUp;
    int 	OverSpeedCount;		// ������� ���������� �������� ��� �������� ���������
    int 	ClientType;
    int		cf = 0;

    int		AcceptTime;
    char    WorkDest[96];           // destination text
    byte    WorkAccept;			    // 0 = �� ����� ������� , 1 = ����� �������
    int     WorkStreetDestinaion;
    int     WorkPointDestinaion;     // ����� ����� ������, ���� ���� ���������
    int     WorkTime;			    // ����� �� ������� �� ������ ��������� �����

    byte    InZone;
    bool    OnStreet;
    bool    InPasZone;
    byte    WorkNow;

    int     Work;
    int     FiredPenalty;
    int     PenaltyCount;
    int     PassCount;

    int		PassStress; // from 0 to 1000
    int		StressOverCount;
    bool 	blink;
    int 	SpeedOff;
    bool    IsPursuit=false;

    time_t  LastT;
};

// �������� ������ �����
class RCTaxi: public RCBaseClass
{
private:
    RCTaxi();   // ����������� ������ (�����������)
    ~RCTaxi();  // ���������� ������ (�����������)
    static RCTaxi* self;

    time_t  acctime;
    int     NumP = 0;

    RCMessage   *msg;   // ����������-��������� �� ����� RCMessage
    RCBank      *bank;  // ����������-��������� �� ����� RCBank
    RCDL        *dl;
    RCStreet    *street;
    RCLight     *lgh;
    RCPolice    *police;

    string Track;
    struct  Taxi_info TrackInf;
    int PointCount;
    int ClientCount;

    struct  Taxi_clients ClientPoints[ MAX_POINTS ];
    byte    StartPointsAdd=0;

    struct  place zone;

    map<byte, TaxiPlayer>players;     // ��������� �������
    Json::Value dialogs;

    void PassAccept( byte UCID );
    void PassAccept2( byte UCID );
    void PassDone( byte UCID );

    void InsimAXM( struct IS_AXM* packet );
    void InsimCNL( struct IS_CNL* packet );   // ����� ���� � �������
    void InsimCON( struct IS_CON* packet );
    void InsimCPR( struct IS_CPR* packet );   // ����� ��������������
    void InsimHLV( struct IS_HLV* packet );
    void InsimMCI( struct IS_MCI* packet );   // ����� � ������� � ����������� � �.�.
    void InsimMSO( struct IS_MSO* packet );   // ����� �������� ���������
    void InsimNCN( struct IS_NCN* packet );   // ����� ����� ����� �� ������
    void InsimNPL( struct IS_NPL* packet );   // ����� ����� �� ������
    void InsimOBH( struct IS_OBH* packet );
    void InsimPLL( struct IS_PLL* packet );   // ����� ���� � �������
    void InsimPLP( struct IS_PLP* packet );   // ����� ���� � �����


    void ReadUser( byte UCID );
    void Save( byte UCID );
    void SaveAll();

    void DeleteMarshal( byte UCID );

    void BtnStress( byte UCID );
    void BtnDistanse( byte UCID , const char* Text);

public:
    static RCTaxi* getInstance();
    // �������� ������� ������
    int init(const char* Dir);


    void ReadConfig(const char *Track); // ������ ������ � ������ "����� ����������"


    bool IfWork(byte UCID);
	void PassDead(byte UCID);
    void PassLoss(byte UCID);
    void Event();


};
#endif // #define _RC_TAXI_H
