/**

Russian Cruise by TurboSnail
denisbatya@yandex.ru
https://vk.com/turbosnail
**/

#ifndef _CRUISE_H_
#define _CRUISE_H_

#include <ctime>
#include <clocale>

#include <signal.h>

#ifndef __linux__
#include <windows.h>
#endif

#include <thread>
#include <stdexcept>

#include "CInsim.h"
#include "RCButtonClickID.h"

#include "DBMySQL.h"


#include "RCCore.h"
#include "RCPizza.h"
#include "RCMessage.h"
#include "RCEnergy.h"
#include "RCBank.h"
#include "RCDrivingLicense.h"
#include "RCAntCheat.h"
#include "RCStreet.h"
#include "RCLight.h"
#include "RCPolice.h"
#include "RCTaxi.h"
#include "RCRoadSign.h"
#include "RCDrugs.h"

#include "RCQuest.h"
#include "RCAutoschool.h"
#include "RCEva.h"


#include "RCItems.h"

#include "COptions.h"
#include "CLog.h"

#include <json/json.h>

int ok = 1;

CInsim *insim;

std::list<RCBaseClass *> classes;

Json::Value 		config;
Json::Reader 		configReader;

#endif
