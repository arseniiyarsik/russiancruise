#ifndef _RC_AUTOSCHOOL_H
#define _RC_AUTOSCHOOL_H

#include "RCBaseClass.h"
#include "RCCore.h"
#include "RCMessage.h"
#include "Items.h"

#define LESSON_INTERVAL 1800 // 5 minutes

struct SchoolPlayer:public GlobalPlayer
{
	string	Lesson = "";
	int     LessonIndex = -1;

	bool	OnPlace = false;
	bool 	Started = false;
	bool	Finished = false;

	bool    CanDoExercise = false;

	unsigned int		CheckPoint = 0;


	float			Rate = 0;           // ������� ������
	float			TempRate = 0;       // ��������� ������� �� ����� ���������� ����������
	bool onSchoolPlace = false;
	bool isHitedHillObj = false;
};

class RCAutoschool : public RCBaseClass
{
	private:
	    RCAutoschool();
		~RCAutoschool();
		static RCAutoschool* self;

		map <byte, SchoolPlayer>players;          // ��������� �������
        map <string, unsigned int>LastTaskTime;   // ����� ���������� ���������� ������

		Json::Value lay;

		RCMessage * msg;
		string Track;

		int* arSchoolAreaX;
		int* arSchoolAreaY;

		void InsimAXM( struct IS_AXM* packet );
		void InsimCNL( struct IS_CNL* packet );   // ����� ���� � �������
		void InsimCON( struct IS_CON* packet );
		void InsimCPR( struct IS_CPR* packet );   // ����� ��������������
		void InsimHLV( struct IS_HLV* packet );
		void InsimMCI( struct IS_MCI* packet );   // ����� � ������� � ����������� � �.�.
		void InsimMSO( struct IS_MSO* packet );   // ����� �������� ���������
		void InsimNCN( struct IS_NCN* packet );   // ����� ����� ����� �� ������
		void InsimNPL( struct IS_NPL* packet );   // ����� ����� �� ������
		void InsimOBH( struct IS_OBH* packet );
		void InsimPLL( struct IS_PLL* packet );   // ����� ���� � �������
		void InsimPLP( struct IS_PLP* packet );   // ����� ���� � �����

		void ShowPanel(byte UCID);
		void HidePanel(byte UCID);
		void NextLesson(byte UCID);
		void CancelLesson(byte UCID);

	public:
		static RCAutoschool* getInstance();
		int init(const char* Dir);
		void ReadConfig(const char* Track);

};

#endif // RCAUTOSCHOOL_H
