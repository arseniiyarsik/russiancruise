#ifndef __DBMYSQL__
#define __DBMYSQL__

using namespace std;

#include <iostream>
#include <list>
#include <map>
#include <queue>
#include <vector>
#include <cstring>
#include <string>
#include <mutex>
#include <exception>

#ifndef __linux__
#include <windows.h>
#endif

#include <mysql/mysql.h>

#include "xString.h"

/**< ���������� - ������������ ������ ���� "�������� ����" -> "�������� ����" */
typedef map< string, xString > DB_ROW;

/**< ���������� - ������������ ������ ���� "�������� ����" -> "�������� ����" */
typedef multimap< string, xString > DB_WHERE;

/**< ���������� - ������ ��������� �� ��������� DB_ROW */
typedef vector<DB_ROW> DB_ROWS;

class DBMySQL
{
private:
    mutex mtx;
	//MYSQL
    MYSQL       dbconn;
    string      host;
    int         port;
    string      username;
    string      password;
    string      database;
    bool        connected;
    map<string, vector<string>> columns;

    /** @brief Get an associative array of the names of database tables with an array of column names
     *
     * @param dbName string
     * @return map<string,vector<string>>
     *
     */
    map<string,vector<string>> collectTables(string dbName);

    /** @brief Get an array of column names
     *
     * @param tableName string
     * @return vector<string>
     *
     */
    vector<string> collectColumns(string tableName);

    int getLastID();

    DBMySQL();
    DBMySQL(const string host, const int port, const string username, const string password, const string database);
	~DBMySQL();
	static DBMySQL* dbInstanse;

public:
    /** @brief Get single instanse of MySQL class
     *
     * @return DBMySQL*
     *
     */
    static DBMySQL* Get();

    /** @brief Get single instanse of MySQL class
     *
     * @param host const char*
     * @param port int
     * @param username const char*
     * @param password const char*
     * @param database const char
     * @return DBMySQL*
     *
     */
    static DBMySQL* Get(const string host, const int port, const string username, const string password, const string database);

    /**< Debug Mode */
    bool debug = false;

    /** @brief Connect to MySQL database
     *
     * @param host const char*
     * @param port int
     * @param username const char*
     * @param password const char*
     * @param database const char*
     * @return bool
     *
     */
    bool connect();

    /** @brief Get last MySQL error
     *
     * @return string
     *
     */
	string getError(){return mysql_error(&dbconn);}

    bool ping();

    /** @brief Select data from MySQL database table
     *
     * @param fields vector<string>
     * @param table string
     * @param where DB_WHERE
     * @return DB_ROWS
     *
     */
    DB_ROWS select(vector<string> fields, string table);
	DB_ROWS select(vector<string> fields, string table, DB_WHERE where);
	bool exec( string query );
	bool exec( const char *query );

    /** @brief Insert fields into table
     *
     * @param table string
     * @param arFields DB_ROW
     * @return unsigned int
     *
     */
	unsigned int insert( string table, DB_ROW arFields);

    /** @brief Update fields into table
     *
     * @param table string
     * @param fields DB_ROW
     * @param where DB_ROW
     * @return bool
     *
     */
	bool update( string table, DB_ROW fields, DB_WHERE where );
};

#endif
