#ifndef RCEVA_H
#define RCEVA_H

#include "RCBaseClass.h"
#include "RCCore.h"
#include "RCMessage.h"
#include "RCDrivingLicense.h"
#include "RCBank.h"
#include "RCTaxi.h"
#include "RCPolice.h"
#include "RCStreet.h"

/** ����� ���� ������ ��������� */
#define PIT_PAY     1000    // ���� � ����, ����� ��� ����������

#define EVC_TAKE    500     // �����������
#define EVC_REPAIR  800     // ����������� � ��������
#define EVC_RESPAWN 1000    // ������������ � ���� � ��������
#define EVC_FASTPIT 2000    // ���������� ���������

#define EVC_POLICE  500     // ����� ����� ��������
/** *************************** */

struct EvoPlayer: public GlobalPlayer
{
    byte PLID;
    byte Zone;
    bool WorkDeal = false;
    bool WorkAccept = false;    // 0 - �� ��������, 1 - ��������

    int NeedToPit = 0;          // ����� �������� ����
    time_t  TimeCallToPit;      // ����� ������ �����

    int LastPointX = 0;         // ���������� ������� ���������� ��� ����������� �������� ����� ��� ����� ���
    int TimeAfk = 0;            // ������� ������� � ���

    int Progress = 0;           // �������� ���������
    int EvaX, EvaY;             // �����, � ������� ��� ������ ����

    /** ���������� */
    int EvcCount = 0;           // ������� ������� � ������� ���������
    int EvcLose = 0;            // �������� ������� (�� �����)
    int EvcFired = 0;           // ���-�� ���������� �� �� ������������
};

class RCEva:public RCBaseClass
{
    private:
        RCEva();
        ~RCEva();
        static RCEva* self;

        RCMessage   *msg;
        RCDL        *dl;
        RCBank      *bank;
        RCTaxi      *taxi;
        RCPolice    *police;
        RCStreet    *street;

        map <byte, EvoPlayer> players;
        void InsimBTC( struct IS_BTC* packet );
        void InsimCNL( struct IS_CNL* packet );     // ����� ���� � �������
        void InsimCPR( struct IS_CPR* packet );     // ����� ��������������
        void InsimMCI( struct IS_MCI* packet );
        void InsimMSO( struct IS_MSO* packet );     // ����� �������� ���������
        void InsimNCN( struct IS_NCN* packet );     // ����� ����� ����� �� ������
        void InsimNPL( struct IS_NPL* packet );     // ����� ����� �� ������
        void InsimPLL( struct IS_PLL* packet );     // ����� ���� � �������
        void InsimPLP( struct IS_PLP* packet );     // ����� ���� � �����

        int     NumP;           // ���-�� ������� �� �������
        int     GetEvaCount();  // ���-�� �����������
        int     GetEvaOnTrackCount();  // ���-�� ����������� ������� ����� ������� �����

        struct  place zone;
        double  Capital; // ������� ���

        void ReadUser(byte UCID);
        void Save(byte UCID);

        void EvcFalse(byte UCID);

        void Pitlane(byte UCID, int PAY = PIT_PAY);

    public:
        static RCEva* getInstance();
        int init(const char* Dir);
        void Event();
        void ReadConfig(const char *Track);
};

#endif // RCEVA_H
