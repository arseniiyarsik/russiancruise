#ifndef _RC_ENERGY_H
#define _RC_ENERGY_H

#include "RCBaseClass.h"
#include "tools.h"
#include "RCMessage.h"
#include "RCBank.h"

#include "tools.h"

#define     MAX_ENERGY 200

struct EnergyPlayer: public GlobalPlayer
{
    byte    Zone;

    /** Energy **/
    float   Energy; // 0-100
    float   leftToFeel = 0;
    float 	feelStep = 0;
    bool	EnergyAlarm = false;
    bool	Lock;
    bool    InSpec = false;

    byte    McDriveStep;
    bool    PayMcDrive = false;
    int     Cost;
    int     Effect;

    time_t  LastT;

    int LockTime = 0;
};

struct energy_info
{
    int     CafeCount;
    int     XCafe[10];
    int     YCafe[10];

    int     McDrive1Count;
    int     XMcDrive1[4];
    int     YMcDrive1[4];

    int     McDrive2Count;
    int     XMcDrive2[4];
    int     YMcDrive2[4];

    int     McDrive3Count;
    int     XMcDrive3[4];
    int     YMcDrive3[4];

    int     McDrive0Count;
    int     XMcDrive0[4];
    int     YMcDrive0[4];
};

class RCEnergy:public RCBaseClass
{
private:
    RCEnergy();
    ~RCEnergy();
    static RCEnergy* self;

    map<byte, EnergyPlayer>players;     // Array of players

    void InsimBTC( struct IS_BTC* packet );
    void InsimCNL( struct IS_CNL* packet );
    void InsimCON( struct IS_CON* packet );
    void InsimCPR( struct IS_CPR* packet );
    void InsimHLV( struct IS_HLV* packet );
    void InsimMCI( struct IS_MCI* packet );
    void InsimMSO( struct IS_MSO* packet );
    void InsimNCN( struct IS_NCN* packet );
    void InsimNPL( struct IS_NPL* packet );
    void InsimOBH( struct IS_OBH* packet );
    void InsimPLL( struct IS_PLL* packet );
    void InsimPLP( struct IS_PLP* packet );

    bool    Lock(byte UCID);
    bool    Unlock(byte UCID);

public:
    static RCEnergy* getInstance();

    RCMessage   *msg;
    RCBank      *bank;

    struct  place zone;

    // �������� ������� ������
    int     init(const char* Dir);
    void    ReadConfig(const char *Track);


    void    Save(byte UCID);
    void SaveAll();

    float   GetEnergy(byte UCID);

    bool    Islocked(byte UCID);

    bool	AddEnergy( byte UCID, float Energy, bool feel = false);
    bool	RemoveEnergy( byte UCID, float Energy);

    bool    LockTo(byte UCID, time_t LockTime = time(nullptr));

    bool    InCafe ( byte UCID );

    void    Event();
};

#endif
