#ifndef RC_ITEMS_H
#define RC_ITEMS_H

#include <map>
#include <string>
#include <CInsim.h>
#include <DBMySQL.h>
#include "RCBaseClass.h"

using namespace std;
class Item
{
private:
    int id; // Привязка к ИД в базе данных
    string name; // "{ParkingTicket}" для русского - "Парковочный талон"
    string description; // "{desc_ParkingTicket}" для русского - "Парковочный талон открывающий доступ на VIP парковку"
    string owner; // кому принадлежит
    string carrier; // кто несёт (тот у кого эта вещь на руках)
    Vec position; // позиция из insim
    string command; //
    string reUsable; // игрок может использовать вешь многократно или нет(по умолчанию) Для не многократн используемых вещей, если useCount = 0 вещь удалятеся Delete().
    int useCount; // если < 0 - вещь использует бесконечно. иначе заданное кол-во раз. Каждый раз при использовании вещи значение -1
    int expires; // Время в секундах. Дата и время когда истекает действие предмета.
    xString ext_info; // служебная часть предмета (для своих надобностей)
    double price; // цена предмета
public:
    Item(int id, string name, string description);

    bool Save();
    bool Delete();

    string getName();
    string getDescription();

    Item* setOwner(xString owner);
    string getOwner();

    Item* setCarrier(xString carrier);
    string getCarrier();

    bool IsUsable();
    bool Use();

    Vec getPos();
    Item* setPos(Vec position);

    int getUseCount();
    Item* setUseCount(int useCount);

    int getExpires();
    Item* setExpires(int expiries);

    xString getExtInfo();
    Item* setExtInfo(xString info);

    double getPrice();
    Item* setPrice(double price);
};

/** @brief Singletone class to work with game items
 */
class Items
{
public:
    /** @brief Get the single instance of Items
     *
     * @return Items*
     *
     */
    static Items* Get();

    /** @brief Add new Item to container
     *
     * @param name xString
     * @param description xString
     * @return int
     *
     */
    int Add(xString name, xString description);

    /** @brief Delete Item from container
     *
     * @param id int
     * @return bool
     *
     */
    bool Delete(int id);

    /** @brief Get pointer to Item by ID
     *
     * @param id int
     * @return Item*
     *
     */
    Item* GetItem(int id);

    /** @brief Get array of Items ids fined by owner
     *
     * @param owner xString
     * @return vector<int>
     *
     */
    vector<int> getItemsByOwner(xString owner);

    /** @brief Get array of Items ids fined by owner and Item name
     *
     * @param owner xString
     * @param name xString
     * @return vector<int>
     *
     */
    vector<int> getItemsByOwner(xString owner, xString name);

    /** @brief Get array of Items ids fined by owner and Item name
     *
     * @param owner xString
     * @param name xString
     * @param carrier xString
     * @return vector<int>
     *
     */
    vector<int> getItemsByOwner(xString owner, xString name, xString carrier);

    /** @brief Get array of Items ids fined by carrier
     *
     * @param carrier xString
     * @return vector<int>
     *
     */
    vector<int> getItemsByCarrier(xString carrier);

    /** @brief Get array of Items ids fined by carrier and Item name
     *
     * @param carrier xString
     * @param name xString
     * @return vector<int>
     *
     */
    vector<int> getItemsByCarrier(xString carrier, xString name);

    /** @brief Get nearest Item by player position and distance
     *
     * @param position Vec
     * @param distance int
     * @return int
     *
     */
    int GetNearestItem(Vec position, int distance);

    /** @brief Load Items from database
     *
     * @return bool
     *
     */
    bool LoadItems();

    /** @brief Save all Items to database
     *
     * @return bool
     *
     */
    bool SaveItems();
protected:
private:
    static Items* _self;
    map<int, Item*> items;

    Items();
    virtual ~Items();
};

#endif // RC_ITEMS_H
