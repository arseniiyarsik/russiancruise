FROM alpine:3.15

WORKDIR /opt/rc
RUN apk add --no-cache mariadb-dev jsoncpp
COPY data data
COPY ./RCMod /usr/local/bin/RCMod
RUN chmod +x /usr/local/bin/RCMod
ENTRYPOINT ['/usr/local/bin/RCMod']
