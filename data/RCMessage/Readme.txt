#define MAX_STRINGS = 20000
//199 functions + 99 messages in function

btn_energy		100
btn_info		200
btn_main		300
btn_panel		400
btn_pogonya		500
btn_shop		600
btn_sirena		700
btn_work		800

case_bfn		900
case_btc		1000
case_btt		1100
case_cnl		1200
case_cpr		1300
case_flg		1400
case_lap		1500
case_pla		3400

case_mci		1600
case_mci_cop	1700
case_mci_energy	1800
case_mci_svetofor	1900

case_mso		2000
case_mso_cop	2100
case_mso_work	2200

case_ncn		2300
case_npl		2400
case_pen		2500
case_pll		2600
case_plp		2700
case_sta		2800
case_vtn		2900

save_user		3000
help_cmds		3100
			3200
			3300

pizza::
deal			4000
undeal		4100
take			4200
done			4300
fire			4400
