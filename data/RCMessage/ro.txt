// ************************
//       ���������
// ************************
currency		"RON"
Energy 			"^%d ^LEnergie: %3.2f^K��"
EnergyArrow		"%s ^K��"
LvlAndSkill 	" ^7Nivelul de condus: ^2%d^7, indemanare: ^2%4.2f"
LvlAndSkill2 	" ^7Nivelul de condus: ^2%d^7, indemanare: ^2%4.2f x%1.1f"
PitSaveGood 	"^2Garaj"
PitSaveNotGood 	"^3Oras"
401 			"^3Magazin"
402 			"^2Cafenea"
404 			"^3Banca"
Dist			"^2%4.3f Km"
Cash 			"%d ^7RON"

ShowCash 		"^7Cash: ^1%d"
ShowLvl			"^7Drive Level: ^1%d^7, skill: ^1%d%%"
ShowCars 		"^2%s ^3%4.0f ^7km"

// ************************
//       ������
// ************************

200 			"^2Masini"
201 			"^2Amenzi"
202 			"^2Despre"

TransfersH 		"^1| ^3LAST TRANSFERS:"

Help1			"^1| ^7RUSSIAN CRUISE"
Help2 			"^1| ^7!lang %s - ^3Alegeti limba"
Help3 			"^1| ^7!info (shift+i) - ^3ecranul cu informatii"
Help4 			"^1| ^7!fines - ^3lista de amenzi"
Help5 			"^1| ^7!cars - ^3lista cu masini permise"
Help6 			"^1| ^7!save - ^3salveaza progresul"
Help7 			"^1| ^7!show - ^3arata-ti progresul"
Help8 			"^1| ^7!pit - ^3tractare pana la pit-uri (^71000 ^3RON)"
Help9 			"^1| ^7!coffee - ^3cafea (^750 ^3RON)"
Help10 			"^1| ^7!redbull - ^3Redbull (^7100 ^3RON)"
Help11			"^1| ^7!tofu - ^3comanda un tofu (^7800 ^3RON)"
Help12 			"^1| ^7!users - ^3operatii cu jucatori"
Help13 			"^1| ^7!911 - ^3Cheama ofiterul de politie la locul accidentului"
Help14			"^1| ^7!help - ^3lista de comenzi"
Help15			"^1| ^C^7!rent CARID - ^3������ ������"

2400 			"^1| ^7Seteaza restrictia la admisie la nu mai putin de^2"
EnergyOver 		"^1| ^7You have run out of power"
2402 			"^1| ^7Ai prea putina putere"
2403 			"^1| ^7Serveste o cafea (^3!coffee^7) sau un Redull (^3!redbull^7)"
2404 			"^1| ^1Ia o masina permisa ^7(^2!cars^7)"

3100 			"^1| ^7Masini disponibile:"
3101 			"^2| ^7Lista ta de amenzi:"

// ************************
//    ������ �������
// ************************

1000 "Trimite bani"
1001 "Trimite un mesaj"

MsgPlFor	"^7Pentru %s"
ItsYou		"^7Esti tu"

SendMoney	"^5| ^7Ai trimis ^8%s ^7%d ^3RON."
GetMoney 	"^5| ^8%s ^7ti-a trimis ^7%d ^3RON."
1101 		"^1| ^7ai introdus o valoare mai mare decat cea pe care o ai in cont."
1102 		"^1| ^7Incearca sa selectezi o cantitate mai mica."
MsgFrom  	"^1| ^7De la %s ^7: ^1%s"

// ************************
//       �������
// ************************

ShopDialog1 	"^2| Magazin"
ShopDialog2 	"^2| ^7!buy XFG/XRG/etc - ^2Cumpara masina"
ShopDialog3 	"^2| ^7!sell XFG/XRG/etc - ^2Vinde masina"
ShopDialog4 	"^2| ^7!tun ECU/TRB/WHT - ^2Cumpara tuningul"
ShopDialog5 	"^2| ^7!untun ECU/TRB/WHT - ^2Vinde tuningul"
1009 			"^1| ^7Nu poti vine masina cat timp esti in ea"
2000 			"^1| ^7Nu esti in magazin!"

You_dont_install_a	"^1| ^7You don't install a %s"
You_have_already_installed_a	"^1| ^7You have already installed a %s"
You_dont_have	"^1| ^7You don't have %s"

SHOP_BUY_SUCCESS	"^2| ^7You bought a %s for %d RUR"
SHOP_SELL_SUCCESS	"^2| ^7You sold the %s for %d RUR"

SHOP_SELL_USAGE		"^2| ^7Use !sell CARID (ex. XRT)"
SHOP_SELL_YOU_HAVENT	"^2| ^7You don't have this car!"
SHOP_SELL_RENTED	"^2| ^7Can't sold rented  car!"
SHOP_BUY_USAGE		"^2| ^7Use !buy CARID (����. XRT)"
SHOP_BUY_WE_HAVENT	"^2| ^7We don't have this car!"

SHOP_BUY_NEED_MONEY	"^2| ^7Need ^1%d ^7RUR."
SHOP_BUY_ALREADY_HAVE	"^2| ^7You already have this car"
SHOP_RENT_USAGE		"^2| ^7Use !rent CARID (ex. XRT)"
SHOP_RENT_SUCCESS	"^2| ^7Yoy rent %s for %d RUR/km"
Nothing_to_sell	"^1| ^7Nothing to sell"
// ************************
//         ����
// ************************

GetMoneyA		"^5| ^7Ai primit %d ^3RON^7"
RemMoneyA		"^5| ^7Ai fost taxat %d ^3RON^7"

PayDone 		"^2| ^7Amenda platita"
NoFine 			"^2| ^7Nu ai de platit aceasta amenda"
NoFines 		"^2| ^7Nu ai de platit nicio amenda"
NoOnBank		"^1| ^7Nu est in banca"
NoManyPay		"^1| ^7Nu ai destui bani"
FinesListH		"^1| ^7You have ^1%d ^7fines"

BankDialog1		"^5| Banca"
BankDialog2		"^5| ^7!credit info N - ^2Informatii despre credit in valoare de N"
BankDialog3		"^5| ^7!credit N (!credit 50000) - ^2Ia un credit in valoare de N"
BankDialog4		"^5| ^7!repay - ^2replateste creditul"
BankDialog5		"^5| ^7!deposit info N - ^2Informatii despre depozitul in valoare de N"
BankDialog6		"^5| ^7!deposit N (!deposit 200000) - ^2Deschide un depozit in valoare de N"
BankDialog7		"^5| ^7!withdraw - ^2Inchide un depozit"
BankDialog8		"^5| ^7!pay N (!pay 13) - ^2Plateste amenda cu ID = N"
BankDialog9		"^1| ^1 ^7%0.0f ^O zi ramasa pentru a sterge creditul"
BankDialog10	"^1| ^1Azi este ultima zi in care poti replati creditul!"
BankDialog11	"^1| ^1AI DEPASIT CREDITUL!"
BankDialog12	"^1| ^7Contul tau este marcat pentru neplata creditului (%d) + penalizare pentru fiecare zi ratata"
BankDialog13	"^5| ^7Ai nevoie de nivelul: ^15"
BankDialog14	"^5| ^7Error. ^7Indica cantitatea de %d ^3RON ^7fata de %d ^3RON^7."
BankDialog15	"^5| ^7informatii despre credit"
BankDialog16	"^5| ^7Ai un credit de %d ^3RON^7. Termenul de plata: %s"
BankDialog17	"^5| ^7Valorea rambursului: %d ^3RON^7."
BankDialog18	"^5| ^7Timpul pana la stergere: ^2%0.0f ^7����."
BankDialog19	"^5| ^7Creditul tau valabil in valoare de %d ^3RON ^7fata de %d ^3RON^7."
BankDialog20	"^5| ^7Creditul este de ^230 %^7, pentru nu mai mult de ^230^7 days."
BankDialog21	"^5| ^7In cazul platii inainte de termenul final, vei fi taxat cu toata valoarea creditului + dobanda."
BankDialog22	"^5| ^7Cantitatea dorita: %d ^3RON^7."
BankDialog23	"^5| ^7Valoarea de ramburs: %d ^3RON^7."
BankDialog24	"^5| ^7Nu poti scoate un credit fara sa inchizi contributia."
BankDialog25	"^5| ^7Ai nevoie de nivelul: ^15"
BankDialog26	"^5| ^7Deja ti-a fost acordat un credit (^3!credit info^7)"
BankDialog27	"^5| ^7Eroare. ^7Indicati valoarea de %d ^3RON ^7fata de %d ^3RON^7."
BankDialog28	"^5| ^7Credit ^1Interzis^7."
BankDialog29	"^5| ^7Cantitatea din contul tau depaseste cantitatea de credit cu mai mult de jumatate."
BankDialog30	"^5| ^7Intr-un credit ^1Interzis^7."
BankDialog31	"^5| ^7Cantitatea din contul tau este mai mica de -50000 3RON^7."
BankDialog32	"^5| ^7Iti va fi emis un credit in valoare de %d ^3RON^7."
BankDialog33	"^5| ^7Nu ai credite."
BankDialog34	"^5| ^7Contul tau nu are destui bani pentru a plati creditul."
BankDialog35	"^5| ^7Ai replatit creditul."
BankDialog36	"^5| ^7Ai nevoie de nivelul: ^120"
BankDialog37	"^5| ^7Eroare. ^7Specifica cantitatea de %d ^3RON."
BankDialog38	"^5| Informatii despre depozite"
BankDialog39	"^5| ^7Ai o contributie in valoare de %d ^3RON^7. Data de deschidere: %s"
BankDialog40	"^5| ^7Cantitatea de acum din contul tau: %d ^3RON^7 (%d ^3RON^7 pe zi)"
BankDialog41	"^5| ^7Termenul depozitului a fost depasit, non-profitul apare."
BankDialog42	"^5| ^7Suma momentului: %d ^3RON^7 (%d ^3RON^7 pe zi)"
BankDialog43	"^5| ^7Inainte de inchiderea contributiei raman: ^2%0.0f ^7zile."
BankDialog44	"^5| ^7Poti contribui cu pana la %d ^3RON."
BankDialog45	"^5| ^7Ai deschis o contributie de ^215 %^7  pe luna, pentru o perioada nu mai mare de ^230^7 zile."
BankDialog46	"^5| ^7Cantitatea dorita a depozitului: %d ^3RON^7."
BankDialog47	"^5| ^7Suma cu dobanda in ^230 ^7 zile: %d ^3RON^7 (%d ^3RON^7 pe zi)."
BankDialog48	"^5| ^7Iti poti inchide contributia nu mai devreme de ^214 ^7 zile dupa deschiderea depozitului."
BankDialog49	"^5| ^7Nu poti deschide un depozit inainte de replatirea unui credit."
BankDialog50	"^5| ^7Ai nevoie de nivelul: ^120"
BankDialog51	"^5| ^7Deja ai deschis o contributie (^3!deposit info^7)."
BankDialog52	"^5| ^7Eroare. ^7Specifica cantitatea de %d ^3RON."
BankDialog53	"^5| ^7Contul tau nu are destui bani pentru a deschide un depozit."
BankDialog54	"^5| ^7Ai deschis o contributie in valoare de %d ^3RON^7."
BankDialog55	"^5| ^7Nu ai nicio contributie."
BankDialog56	"^5| ^7Ti-ai inchis contributia."
BankDialog57	"^5| ^7Banca primeste o taxa de penalizare in valoare de %d ^3RON^7."
BankDialog58	"^5| ^1Atentie! ^7Nu au trecut inca 14 zile de la deschiderea depozitului."
BankDialog59	"^5| ^7Enter ^3!withdraw yes ^7pentru a inchide contributia. Banca va primi o taxa de penalizare ^25 %^7."
BankDialog60	"^5| ^7Ai inchis contributia."

// ************************
//          ����
// ************************

4000 			"^3|^7 Te voi duce la munca"
4001 			"^3|^7 Nu muncesti aici"
4002 			"^3|^7 Nu muncesti cu mine, pleaca"

1600 			"^3|^2 Tofu Fujiwara ^7Magazin"
1601 			"^3|^2 !deal ^7- Ia-ti o slujba"
1602 			"^3|^2 !undeal ^7- Demisioneaza"
FreeEat			"^3| ^2!eat ^7- Mananca tofu gratuit"

2201 			"^3| ^7Ai o comanda de preluat, vino la magazinul de tofu!"
2202 			"^7Preia comanda"

NoTofuCars 		"^3| ^7Momentan nu sunt muncitori liberi"
2203 			"^3| ^7Momentan nu sunt muncitori liberi"
DelTofu 		"^3| ^7Rezervare acceptata, asteapta pana cand iti este livrat tofu-ul"
UAreDel 		"^3| ^7Ai facut deja o comanda, asteapta pana iti este livrat tofu-ul"
1604 			"^3|^7 Tofu-ul dvs.  Domnule."
NotLesEn		"^3| ^7Puterea ta trebuie sa fie mai mica de 80%"
NoTofuWhWork	"^3| ^7Nu poti comanda un tofu pentru ca lucrezi aici"

4100 			"^3| ^7Esti concediat"
4101 			"^3| ^7Ai pierdut o comanda - esti concediat^1!!!"
4102 			"^3| ^7Ai nevoie de o masina: ^1UF 1000 ^7or ^1SEAZ 11113"
4103 			"^3| ^7Ai nevoie de  prefixul ^1[^7PIZZA^1] or ^1[^7TOFU^1]"
4104 			"^3| ^7Nu sunt pozitii valabile"

4200 			"^7Livrarea numarul %s, %s"
4201 			"^3|^7 Livrare catre jucatorul %s"
4202 			"^3|^7 Mai intai ia comanda asta"
4203 			"^3|^7 Asteapta pana te sun"
4204 			"^7Comanda produse in magazin"
4205 			"^3| ^7Multumesc pentru ca ai comandat produsele"
4210			"^3| ^8%s^7, Comanda curenta: ^3%s^7, total: ^3%d"
4300 			"^3|^7 Livrat"

4206			"^3| ^7Magazinul capital: ^7%9.2f ^3RON"
4207			"^3| ^7Depozit: apa ^3%0.0f ^7l, faina ^3%0.0f ^7kg, legume ^3%0.0f ^7kg, branza ^3%0.0f ^7kg"
4208			"^3| ^7Fara muncitori"
4209			"^3| ^7Muncitori: ^3%d/%d"

4211			"^3| ^7Tofu din partea casei"
4212 			"^3| ^7Ai muncit aici indeajuns de mult incai sa primesti mese gratis"
4213 			"^3| ^7Poti manca pe gratis in instituria noastra"

// ************************
//         �����
// ************************

TaxiDialog1			"^6| ^3Taxi Radriges"
TaxiDialog2			"^6| ^2!deal ^7- take a job"
TaxiDialog3			"^6| ^2!undeal ^7- leave the job"

TaxiDeal			"^6| ^7You accepted, go to work"
TaxiAlrdUndeal		"^6| ^7You do not work here"
TaxiUndeal			"^6| ^7You're fired!"
TaxiNeedLvl			"^6| ^7Need level: ^120"
TaxiFiredPenalty	"^6| ^7You were punished to %d minutes! do not want deal with you."
TaxiAlrdWork		"^6| ^7You already hired"
TaxiNeedPref		"^6| ^7Need a nickname with the prefix %s"
TaxiWrongCar		"^6| ^7You can not work on this car"

TaxiStatH			"^6| ^7Statistic taxi work of ^9%s ^7(^9%s^7)"
TaxiStatNoWork		"^6| ^7No Worked"
TaxiStatLine1		"^6| ^7Current work: %d"
TaxiStatLine2		"^6| ^7All accepted: %d"
TaxiStatLine3		"^6| ^7All lost: %d"
TaxiStatNotF		"^6| ^7Player ^9%d ^C^7not found"

TaxiOnStreet		"^6| ^7Stop after %0.0f meters"
TaxiAccept1			"^7Take away the client on %s"
TaxiAccept2			"^6| ^7Take away the client on %s"
TaxiAccept11		"^6| ^C^7������ ������� �� %s"
TaxiAccept22		"^6| ^7Take the client on %s"
TaxiDead			"^6| ^7You killed your client - ^1FIRED!"
TaxiPll 			"^6| ^7You lost a client"

// ************************
//        �������
// ************************

1002 			"^7Amenda"
1003 			"^7Anuleaza amenda"
1004 			"Incepi urmarirea"
1005 			"Opreste urmarirea"

RideButton 		"^4URMARIRE IN SPATELE TAU"
RightAndStop 	"^1Ramai pe marginea drumului si opreste"
ArestButton		"^1ESTI PRINS"
1702			"^1 este arestat"
1706 			"^7departe de urmarire"

ArestedMin		"^2| ^8%s ^1arestat pentru ^7%d ^1min"
Reliaved		"^2| ^8%s ^2eliberat din arest"
YouHaveAr		"^2| ^7Esti arestat. Asteapta pana la finalul arestului: ^1%d ^7min"
YouUndArest		"^1Esti arestat"

PogonyaOn 		"^2| ^7Urmarirea lui %s ^7(%s^7)"
PogonyaOff 		"^2| ^7Opreste urmarirea lui %s ^7(%s^7)"
GiveFine 		"^2| ^8%s ^7ti-a scris o amenda: %s"
GiveFine2		"^1| ^7Esti amendat: %s"
GiveFine3		"Esti amendat"
AddFine 		"^2| ^7Esti amendat %s^7 : %s"
DeletedFine 	"^2| ^8%s ^7ti-a anulat amenda: %s"
DelFine			"^2| ^7Ti-ai anulat o amenda %s ^7: %s"
Speeding 		"^2| ^8%s ^7a depasit limita de viteza cu ^1%d ^3km/h^7, %s"
FinesButton		"Amenzi"

1703 			"^2| ^7Te rog seteaza Urmarirea in modul Off"
CopFalse 		"^2| ^7Nu esti un politist"
CopTrue 		"^2| ^7Esti politist"
1302 			"^2| ^7Nu esti un politist"
1303 			"^2| ^7Politistii nu pot lucra pe taxiuri"

3400 			"^1| ^7Ai mai mult de 10 amenzi. Plateste-le"

2100 			"^2| ^7Sirena Off"
2101 			"^2| ^7Sirena On"
2102			"^2| ^7Toti politistii rutieri sunt ocupati"
WherePog		"^2| ^1Nu apela la DGP in timpul urmaririi"
WhereDtp		"^2| ^1Deja ai cauzat DGP sa astepte"
NoDtpWhenSp		"^2| ^1Nu poti apela la DGP cat conduci"
Compens 		"^2| ^7Ai primit o compensatie in valoare de %0.0f ^3RON"
CompensPl 		"^2| ^9%s ^7a primit o compensatie in valoare de %0.0f ^3RON"
2106			"^2| ^7Nu se poate intampla mai repede de o data la 5 minute"
2107			"^2| ^7Aplicatia ta va fi luata in considerare in cadrul a 5 minute"
2108			"^2| ^7Nu parasi locul accidentului pana la sosirea ofiterului DGP"
2109			"^2| ^1Ai un apel pentru un accident de la%s"
2110			"^2| ^7Lista cu aplicatii este plina"
2111			"^2| ^7Poti sa nu folosesti radarul"
2112			"^2| ^7Posi sa folosesti radarul cat analizezi accidentul"
2113			"^2| ^7Nu ai castigat dupa schimbarea anterioara"
2114			"^2| %s ^7inchide-ti oferta pentru accident"
2115			"^2| ^7Oferta ^8%s ^7pentru accident a fost inchisa"
2116			"^2| ^7Nu face mai mult de o aplicatie in acelasi timp"
2117			"^2| %s ^7accepta aplicatia ta pentru considerentul accidentului"
2118			"^2| %s ^7a primit o cerere ^8%s ^7catre accident"
2119			"^2| ^7Nu poti incepe o urmarire cat timp analizezi un accident"
2120			"^2| ^1Ai ratat un apel pentru un accident"
2121			"Aplicatia este acceptata - %s, %s"
2122			"^2Oferta ^8%s ^2este luata�in considerare - ^1 inchide"
2123			"Apel catre un accident primit - %s^8, %s"
2124			"%s ^7 - %s, ^1este arestat"
RadarOn 		"^2| ^7Radarul este On"
RadarOff 		"^2| ^7Radar ul este Off"

AddBarier               "^2| %s ^7^Lpune o bariera pentru  %s"
DelBarier               "^2| ^7^LBariera este scoasa"
addBarier		" ^L^1a pus o bariera"
delBarier		" ^L^1scoate bariera"


2600 			"^2| ^7Grija la spectatori in timpul unei urmariri. Amenda - %d ^3RON"
2700 			"^2| ^7Grija la spectatori in timpul unei urmariri. Amenda - %d ^3RON"
EndOfRadar		"^2| ^7Nu sunt radare valabile"

// ************************
//        ������
// ************************

GetLvl 			"^5| ^8%s ^1ia ^3%d ^1nivel"
1500 			"^1| ^7Bonus: %d ^3RON"
bonus_button            "^2 %d ^7RON ^3|^2 %2.1f^K��"
AddSkill		"^1| ^2+ ^3%2.2f%% ^7indemanare"
RemSkill 		"^1| ^1- ^3%2.2f%% ^7indemanare"

1801 			"^1| ^7Ia cafea (^3!coffee^7) sau Redbull (^3!redbull^7)"

2602 			"^1| ^7Grija la spectatorii care stau unde nu trebuie. Amenda - reset bonus"
2702 			"^1| ^7Grija la spectatorii care stau unde nu trebuie. Amenda - reset bonus"

EnergyFull		"^1| ^7Enenrgie plina"
EnergyIsFeeling	"^1| ^7Bea portia anterioara"
2001 			"^1| ^7Nu ai destui bani"
2002 			"^1| ^7Nu esti la cafenea"
flood 			"^1| ^7Flood! Amenda: %d ^3RON"
swear 			"^1| ^7Injurii%s. Amenda: %d ^3RON"

2104 			"^2| ^7Am nevoie de un parametru"
2105 			"^2| ^7Parametru gresit"

3000 			"^1| ^7Statisticile tale sunt salvate"
SAVE_MANY_TIME		"^1| ^7Can't be saved so often"

CONFIG_RELOADED		"^1| ^3Russian Cruise: ^7Config reloaded"
NEED_LVL	        "^2| ^7Need levewl: ^1%d"


ITEMS_RELOADED		"^1| ^3Russian Cruise: ^7Items reloaded"

// ************************
//     �� ������ ���
// ************************

600 	"^7Show Auto"
601 	"^7Service"
602 	"^7Cumpara"
603 	"^7Vinde"
604 	"^1Inchide"
1800 	"^1| ^7Ai cheltuit toata puterea!"
1802 	"^1| ^7De asemenea recomand sa te relaxezi intr-o cafenea"

// ************************
//     ������ ���� 4 �������
// ************************
help_tab	"^2Primul ajutor"
info_help_1	"^7Cum sa iti setezi restrictia voluntara la admisie:"
info_help_2	"^71. Du-te la pit-uri"
info_help_3	"^72. Creaza un nou set de setari petru masina (daca il folosesti pe cel implicit)",
info_help_4	"^73. Gaseste ^tInfo^t tab. Apasa",
info_help_5	"^74. Gaseste coloana din dreapta ecranului cu sliderele",
info_help_6	"^75. Gaseste sliderul ^tVoluntary Intake Restriction^t",
info_help_7	"^76. Seteaza sliderul la 45%%"

//***************************
//        McDrive
//***************************

MdHeader 				"^7Autocafe ^3McDrive"

MdChose1 				"^7Milkshake (^210%^7, 99 ^3RUR^7)"
MdChose2 				"^7Vegetable salad (^220%^7, 199 ^3RUR^7)"
MdChose3 				"^7Burger (^250%^7, 499 ^3RUR^7)"
MdChose4 				"^7French fries (^275%^7, 749 ^3RUR^7)"
MdChose5 				"^7Bizneslanch (^290%^7, 899 ^3RUR^7)"

MdChoose				"^3| ^7Order is accepted, drive to the next window"
MdPay 					"^3| ^7With you %d ^3RUR"
MdTake 					"^3| ^7Here is your order. Have a good trip!"

MdNotChose				"^3| ^7You do not choose"
MdNotPay 				"^3| ^7You have not paid order"

//***************************
//        Evacuator
//***************************

sto_head		"^3| ^2Maintenance Service"
stodeal			"^3| ^2!deal ^7- take a job"
stoundeal		"^3| ^2!undeal ^7- leave the job"
	
EvcAlrdDeal		"^3| ^7Have you hired"
EvcNoJob		"^3| ^7Free no vacancies"
EvcNeedCar		"^3| ^7Need a car: ^1UFR, XFR"
EvcNeedPref		"^3| ^7I need a nickname with the prefix ^3[^1EV�^3]"
EvcDeal			"^3| ^7You received"
EvcUndeal		"^3| ^7You"re fired"
EvcAlrdUndeal	"^3| ^7You"re here not work"
	
EvcCapital		"^3| ^7Capital MS: %9.2f ^3RUR"
EvcNoWorkers	"^3| ^7Workers do not have"
EvcWorkCount	"^3| ^7Workers: %d/%d"
	
EvcCntInPit		"^3| ^7You can not call a tow truck in pits"
EvcCntSpeed		"^3| ^7You can not call evc on the move"
EvcCntCall		"^3| ^7You already called evc, wait for the arrival of the employee"
EvcCntTime		"^3| ^7You can not call evc more than once every 3 minutes"
	
EvcCall			"^3| ^7You call evc, wait for the arrival of the employee"
EvcCall2		"^3| ^7Received application evacuation from ^9%s ^7(%s)"
	
EvcAlrdCall		"^3| ^7This player has already called evc"
EvcPoliceCall	"^3| ^7You call evc to %s"
EvcPoliceCall2	"^3| %s ^7called the evc, wait for the arrival of the employee"
	
EvcWndTitle		"Evacuation service running on the server"
EvcTake			"Turn the car - ^7%d ^3RUR"
EvcRepair		"Turn and repair - ^7%d ^3RUR"
EvcRespawn		"Evacuate the pits - ^7%d ^3RUR"
EvcFastPit		"Instant transition into the pits - ^1%d ^3RUR"
EvcWndCancel	"Cancel"
	
EvcLeave		"^3| ^7The server is no longer evc, your car is sent into the pits"
EvcAuto			"^3| ^7Automatic evacuation"
EvcAuto2		"^3| ^9%s ^7evacuated automatically"
EvcDown			"^3| ^7Evc were unable to process your application, your car free of charge sent to the pits"
EvcDown2		"^3| ^7You missed the application for the evacuation of the car %s ^7at your expense"
	
EvcGetCall		"^7Calling from %s ^7to ^3%s ^7(%s%d m^7) - ^1%02d:%02d"
EvcGetEv		"^2Evacuation %s - ^1%02d:%02d"
EvcGetEv2		"^2Evacuation: "
	
EvtGetTake		"^3| ^7You turned the ^9%s"
EvtGetTake2		"^3| ^9%s ^7turned your car"
EvcGetRepair	"^3| ^7You turned and repaired ^9%s"
EvcGetRepair2	"^3| ^9%s ^7turned and repaired your car"
EvcGetRespawn	"^3| ^7You evacuated ^9%s ^7to the pits"
EvcGetRespawn2	"^3| ^9%s ^7evacuate you to the pits"
EvcGetPolice	"^3| ^7You evacuated ^9%s ^7to the pits"
EvcGetPolice2	"^3| ^9%s ^7evacuate you to the pits"
	
EvcAfkFired		"^3| ^7You"re standing too long without moving. You"re fired."
EvcFine			"^3| ^7Penalty for false alarm evacuation service"
EvcFine2		"^3| ^9%s ^7fined for false alarm evacuation service"

//**********************
// Driving school
//**********************

box-parking				"Box parking"
hill					"Hill"
parallel-parking		"Parallel parking"
chicane					"Chicane"
u-turn					"U turn"

SCHOOL_FAIL				"^6| ^7Wasted"
SCHOOL_WAIT				"^6| ^7Wait about %d minutes"
SCHOOL_SUCCESS			"^6| ^7Congratulations!!! You get Driver's License category ^3%s"

SCHOOL_ALREADY_EXAM		"^6| ^7You are already in the exam"
SCHOOL_ALREADY_GOT		"^6| ^7You are already got drivers license category %s"
SCHOOL_GOTO_EXAM		"^6| ^7Go to the exercise: %s"

SCHOOL_AREA_WELCOME		"^6| ^7Welcome to Driving school."
SCHOOL_AREA_WELCOME2	"^6| ^7Here you can take an exam to obtain a Category %s driver's license"
SCHOOL_AREA_START		"^6| ^3!start ^7- start examination"

SCHOOL_EXERCISE_PREPARE	"^6| ^7Please go to start position then stop!"
SCHOOL_EXERCISE_START	"^6| ^7Start to do an exercise!"

SCHOOL_PANEL_EXERCISE	"^7Exercise: ^3%s"
SCHOOL_PANEL_CHECKPOINT	"^7Checkpoint: ^3%d^7/^3%d"